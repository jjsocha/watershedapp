package com.watershedapp.db;

public class ObservationSmell {
	public static final String TABLE_NAME = "observation_smell";

	// Column names
	public static final String OBSERVATION_ID = "obseravtion_id";
	public static final String NO_ODOR = "smell_no_odor";
	public static final String ROTTEN_EGGS = "smell_rotten_eggs";
	public static final String GASOLINE_OIL = "smell_gasoline_oil";
	public static final String CHEMICAL = "smell_chemical";
	public static final String CHLORINE = "smell_chlorine";
	public static final String SEWAGE = "smell_sweage";
	public static final String ROTTING = "smell_rotting";
	public static final String FISHY = "smell_fishy";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
					OBSERVATION_ID + " integer primary key, " +
					NO_ODOR + " integer not null, " +
					ROTTEN_EGGS + " integer not null, " +
					GASOLINE_OIL + " integer not null, " +
					CHEMICAL + " integer not null, " +
					CHLORINE + " integer not null, " +
					SEWAGE + " integer not null, " +
					ROTTING + " integer not null, " +
					FISHY + " integer not null" +
				");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean noOdor;
	public boolean rottenEggs;
	public boolean gasolineOil;
	public boolean chemical;
	public boolean chlronine;
	public boolean sewage;
	public boolean rotting;
	public boolean fishy;
}
