package com.watershedapp.db;

public class ObservationAlgeaColor {
	public static final String TABLE_NAME = "observation_algea_color";
	
	// Column names
	public static final String OBSERVATION_ID = "observation_id";
	public static final String LIGHT_GREEN = "algea_light_green";
	public static final String DARK_GREEN = "algea_dark_green";
	public static final String BROWN = "algea_brown";
	public static final String RED = "algea_red";
	public static final String ORANGE = "algea_orange";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
				OBSERVATION_ID + " integer primary key, " +
				LIGHT_GREEN + " integer not null, " +
				DARK_GREEN + " integer not null, " +
				BROWN + " integer not null, " +
				RED + " integer not null, " +
				ORANGE + " integer not null" +
			");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean lightGreent;
	public boolean darkGreen;
	public boolean brown;
	public boolean red;
	public boolean orange;
}
