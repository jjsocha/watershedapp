package com.watershedapp.db;

public class ObservationMaterial {
	public static final String TABLE_NAME = "observation_material";

	// Column names
	public static final String OBSERVATION_ID = "obseravtion_id";
	public static final String SAND = "material_sand";
	public static final String GRAVEL = "material_gravel";
	public static final String BOULDERS = "material_boulders";
	public static final String BEDROCK = "material_bedrock";
	public static final String SILT = "material_silt";
	public static final String OTHER = "material_other";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
				OBSERVATION_ID + " integer primary key, " +
				SAND + " integer not null, " +
				GRAVEL + " integer not null, " +
				BOULDERS + " integer not null, " +
				BEDROCK + " integer not null, " +
				SILT + " integer not null, " +
				OTHER + " integer not null" +
			");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean sand;
	public boolean gravel;
	public boolean boulders;
	public boolean bedrock;
	public boolean silt;
	public boolean other;
}
