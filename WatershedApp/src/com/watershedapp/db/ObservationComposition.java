package com.watershedapp.db;

public class ObservationComposition {
	public static final String TABLE_NAME = "observation_composition";
	
	// Column names
	public static final String OBSERVATION_ID = "observation_id";
	public static final String SAND = "composition_sand";
	public static final String GRAVEL = "composition_gravel";
	public static final String BOULDERS = "composition_boulders";
	public static final String SILT = "composition_silt";
	public static final String OTHER = "composition_other";
	public static final String MAN_MADE_CEMENT = "compostion_man_made_cement";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
				OBSERVATION_ID + " integer primary key, " +
				SAND + " integer not null, " +
				GRAVEL + " integer not null, " +
				BOULDERS + " integer not null, " +
				SILT + " integer not null, " +
				OTHER + " integer not null, " +
				MAN_MADE_CEMENT + " integer not null" +
			");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean sand;
	public boolean gravel;
	public boolean boulders;
	public boolean silt;
	public boolean other;
	public boolean manMadeCement;
}
