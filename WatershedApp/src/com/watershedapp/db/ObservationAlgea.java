package com.watershedapp.db;

public class ObservationAlgea {
	public static final String TABLE_NAME = "observation_algea";
	
	// Column names
	public static final String OBSERVATION_ID = "observation_id";
	public static final String NOT_PRESENT = "algea_not_present";
	public static final String PRESENT_IN_SPOTS = "algea_present_in_spots";
	public static final String ATTACHED_TO_ROCKS = "algea_attached_to_rocks";
	public static final String EVERYWHERE = "algea_everywhere";
	public static final String FLOATING = "algea_floating";
	public static final String MATTED_ON_THE_STREAMBED = "algea_matted_on_thestreambed";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
				OBSERVATION_ID + " integer primary key, " +
				NOT_PRESENT + " integer not null, " +
				PRESENT_IN_SPOTS + " integer not null, " +
				ATTACHED_TO_ROCKS + " integer not null, " +
				EVERYWHERE + " integer not null, " +
				FLOATING + " integer not null, " +
				MATTED_ON_THE_STREAMBED + " integer not null" +
			");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + ObservationAlgea.TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean notPresent;
	public boolean presentInSpots;
	public boolean attachedToRocks;
	public boolean everywhere;
	public boolean floating;
	public boolean mattedOnTheStreamBed;
}
