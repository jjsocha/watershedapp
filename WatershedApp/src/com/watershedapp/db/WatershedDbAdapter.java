package com.watershedapp.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class WatershedDbAdapter {
	// Database name and version
	private static final String DATABASE_NAME = "WatershedApp"; 
	private static final int DATABASE_VERSION = 2;

	private static final String TAG = "WatershedDbAdapter";
	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;
	private final Context mCtx;

	private static class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(UserLogin.CREATE_TABLE);
			db.execSQL(Observation.CREATE_TABLE);
			db.execSQL(ObservationAlgea.CREATE_TABLE);
			db.execSQL(ObservationAlgeaColor.CREATE_TABLE);
			db.execSQL(ObservationBarriers.CREATE_TABLE);
			db.execSQL(ObservationColor.CREATE_TABLE);
			db.execSQL(ObservationComposition.CREATE_TABLE);
			db.execSQL(ObservationLandUse.CREATE_TABLE);
			db.execSQL(ObservationMaterial.CREATE_TABLE);
			db.execSQL(ObservationSmell.CREATE_TABLE);
			db.execSQL(ObservationStreamUse.CREATE_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
					+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS UserLogin");
			db.execSQL("DROP TABLE IF EXISTS observation");
			db.execSQL("DROP TABLE IF EXISTS observation_algae");
			db.execSQL("DROP TABLE IF EXISTS observation_algae_color");
			db.execSQL("DROP TABLE IF EXISTS observation_barriers");
			db.execSQL("DROP TABLE IF EXISTS observation_color");
			db.execSQL("DROP TABLE IF EXISTS observation_composition");
			db.execSQL("DROP TABLE IF EXISTS observation_land_use");
			db.execSQL("DROP TABLE IF EXISTS observation_material");
			db.execSQL("DROP TABLE IF EXISTS observation_smell");
			db.execSQL("DROP TABLE IF EXISTS observation_stream_use");
			onCreate(db);
		}
	}

	public WatershedDbAdapter(Context context){
		this.mCtx = context;
	}

	public WatershedDbAdapter open(){
		mDbHelper = new DatabaseHelper(mCtx);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close(){
		mDbHelper.close();
	}

	public long addUser(String userName, String password){
		ContentValues values = new ContentValues();
		values.put(UserLogin.USERNAME, userName);
		//TODO: Try authenticating with server
		values.put(UserLogin.LOGINTOKEN, "");

		return mDb.insert(UserLogin.TABLE_NAME, null, values);
	}

	public boolean delteAllUsers(){
		return mDb.delete(UserLogin.TABLE_NAME, "", null) > 0;
	}

	public UserLogin getCurrentUser(){
		Cursor mCursor = 
				mDb.rawQuery("SELECT * FROM "+UserLogin.TABLE_NAME+" LIMIT 1", null);
		if(mCursor != null && mCursor.getCount() > 0){
			Log.d(TAG, "Gotten user succesfully");
			mCursor.moveToFirst();

			UserLogin user = new UserLogin();
			user.Id = mCursor.getInt(mCursor.getColumnIndex(UserLogin.ROWID));
			user.Username = mCursor.getString(mCursor.getColumnIndex(UserLogin.USERNAME));
			user.Token = mCursor.getString(mCursor.getColumnIndex(UserLogin.LOGINTOKEN));

			return user;
		}
		mCursor.close();
		Log.d(TAG, "No user in database");
		return null;
	}

	public ArrayList<Observation> getAllObservations(){
		Cursor mCursor = mDb.rawQuery(
				"SELECT " + Observation.ROW_ID + " FROM " + Observation.TABLE_NAME
				, null);
		ArrayList<Observation> observations = new ArrayList<Observation>();
		if(mCursor.moveToFirst()){
			do
			{
				observations.add(getObservation(mCursor.getInt(0)));
			}while(mCursor.moveToNext());
		}
		return observations;
	}
	public void deleteImage(String name, int imageNumber, long id) {
		ContentValues values = new ContentValues();
		
		switch(imageNumber){
		case 1: values.putNull(Observation.IMAGE_URI_CAT_1);
		break;
		case 2: values.putNull(Observation.IMAGE_URI_CAT_2); 
		break;
		case 3: values.putNull(Observation.IMAGE_URI_CAT_3);
		break;
		case 4: values.putNull(Observation.IMAGE_URI_CAT_4);
		break;
		}
		mDb.update(Observation.TABLE_NAME, values, Observation.ROW_ID + "=?", new String[]{"" + id});
	}
	public void deleteObservation(long id){
		mDb.delete(Observation.TABLE_NAME, Observation.ROW_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationAlgea.TABLE_NAME, ObservationAlgea.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationAlgeaColor.TABLE_NAME, ObservationAlgeaColor.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationBarriers.TABLE_NAME, ObservationBarriers.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationColor.TABLE_NAME, ObservationColor.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationComposition.TABLE_NAME, ObservationComposition.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationLandUse.TABLE_NAME, ObservationLandUse.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationMaterial.TABLE_NAME, ObservationMaterial.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationSmell.TABLE_NAME, ObservationSmell.OBSERVATION_ID + "=?", new String[]{"" + id});
		mDb.delete(ObservationStreamUse.TABLE_NAME, ObservationStreamUse.OBSERVATION_ID + "=?", new String[]{"" + id});
	}
	
	public Observation getObservation(long id)
	{
		Observation observation = new Observation();
		// Get info form observation table
		Cursor mCursor = mDb.rawQuery(Observation.SELECT_BY_ID , new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.anonymous = intToBool(mCursor.getInt(mCursor.getColumnIndex(Observation.ANONYMOUS)));
			observation.comments = mCursor.getString(mCursor.getColumnIndex(Observation.COMMENTS));
			observation.date = mCursor.getString(mCursor.getColumnIndex(Observation.DATE));
			observation.fishPrescence = mCursor.getString(mCursor.getColumnIndex(Observation.FISH_PRESENCE));
			observation.fishTypes = mCursor.getString(mCursor.getColumnIndex(Observation.FISH_TYPES));
			observation.imageUriCat1 = mCursor.getString(mCursor.getColumnIndex(Observation.IMAGE_URI_CAT_1));
			observation.imageUriCat2 = mCursor.getString(mCursor.getColumnIndex(Observation.IMAGE_URI_CAT_2));
			observation.imageUriCat3 = mCursor.getString(mCursor.getColumnIndex(Observation.IMAGE_URI_CAT_3));
			observation.imageUriCat4 = mCursor.getString(mCursor.getColumnIndex(Observation.IMAGE_URI_CAT_4));
			observation.leftBank = mCursor.getString(mCursor.getColumnIndex(Observation.LEFT_BANK));
			observation.locationLat = mCursor.getString(mCursor.getColumnIndex(Observation.LOCATION_LAT));
			observation.locationLong = mCursor.getString(mCursor.getColumnIndex(Observation.LOCATION_LONG));
			observation.pipes = mCursor.getString(mCursor.getColumnIndex(Observation.PIPES));
			observation.pipeSecretion = mCursor.getString(mCursor.getColumnIndex(Observation.PIPE_SECRETION));
			observation.repAmphPresent = mCursor.getString(mCursor.getColumnIndex(Observation.REP_AMPH_PRESENT));
			observation.rightBank = mCursor.getString(mCursor.getColumnIndex(Observation.RIGHT_BANK));
			observation.rowId = mCursor.getInt(mCursor.getColumnIndex(Observation.ROW_ID));
			observation.stateMuni = mCursor.getString(mCursor.getColumnIndex(Observation.STATE_MUNI));
			observation.trash = mCursor.getString(mCursor.getColumnIndex(Observation.TRASH));
			observation.treeSahde = mCursor.getString(mCursor.getColumnIndex(Observation.TREE_SHADE));
			observation.waterbody = mCursor.getString(mCursor.getColumnIndex(Observation.WATERBODY));
			observation.waterMovement = mCursor.getString(mCursor.getColumnIndex(Observation.WATER_MOVEMENT));
			observation.watershed = mCursor.getString(mCursor.getColumnIndex(Observation.WATERSHED));
			observation.waterSpeed = mCursor.getString(mCursor.getColumnIndex(Observation.WATER_SPEED));
		}
		mCursor.close();

		// Get info from ObservationAlgea table
		mCursor = mDb.rawQuery(ObservationAlgea.SELECT_BY_ID , new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.Algea.attachedToRocks = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgea.ATTACHED_TO_ROCKS)));
			observation.Algea.everywhere = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgea.EVERYWHERE)));
			observation.Algea.floating = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgea.FLOATING)));
			observation.Algea.mattedOnTheStreamBed = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgea.MATTED_ON_THE_STREAMBED)));
			observation.Algea.notPresent = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgea.NOT_PRESENT)));
			observation.Algea.presentInSpots = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgea.PRESENT_IN_SPOTS)));
			observation.Algea.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationAlgea.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationColor table
		mCursor = mDb.rawQuery(ObservationAlgeaColor.SELECT_BY_ID , new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.AlgeaColor.brown = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgeaColor.BROWN)));
			observation.AlgeaColor.darkGreen = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgeaColor.DARK_GREEN)));
			observation.AlgeaColor.lightGreent = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgeaColor.LIGHT_GREEN)));
			observation.AlgeaColor.orange = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgeaColor.ORANGE)));
			observation.AlgeaColor.red = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationAlgeaColor.RED)));
			observation.AlgeaColor.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationAlgeaColor.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationBarriers table
		mCursor = mDb.rawQuery(ObservationBarriers.SELECT_BY_ID, new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.Barriers.bridges = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationBarriers.BRIDGES)));
			observation.Barriers.dams = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationBarriers.DAMS)));
			observation.Barriers.waterfalls = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationBarriers.WATERFALLS)));
			observation.Barriers.woodyDebris = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationBarriers.WOODY_DEBRIS)));
			observation.Barriers.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationBarriers.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationColor table
		mCursor = mDb.rawQuery(ObservationColor.SELECT_BY_ID , new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.Color.balck = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.BLACK)));
			observation.Color.brown = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.BROWN)));
			observation.Color.clear = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.CLEAR)));
			observation.Color.foamy = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.FOAMY)));
			observation.Color.green = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.GREEN)));
			observation.Color.muddy = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.MUDDY)));
			observation.Color.oily = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.OILY)));
			observation.Color.other = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationColor.OTHER)));
			observation.Color.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationColor.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationComposition table
		mCursor = mDb.rawQuery(ObservationComposition.SELECT_BY_ID, new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.Composition.boulders = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationComposition.BOULDERS)));
			observation.Composition.gravel = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationComposition.GRAVEL)));
			observation.Composition.manMadeCement = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationComposition.MAN_MADE_CEMENT)));
			observation.Composition.other = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationComposition.OTHER)));
			observation.Composition.sand = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationComposition.SAND)));
			observation.Composition.silt = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationComposition.SILT)));
			observation.Composition.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationComposition.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationLandUse table
		mCursor = mDb.rawQuery(ObservationLandUse.SELECT_BY_ID, new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.LandUse.construction = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.CONSTRUCTION)));
			observation.LandUse.factories = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.FACTORIES)));
			observation.LandUse.farming = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.FARMING)));
			observation.LandUse.forest = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.FOREST)));
			observation.LandUse.logging = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.LOGGING)));
			observation.LandUse.mining = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.MINING)));
			observation.LandUse.pasture = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.PASTURE)));
			observation.LandUse.poultrySwine = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.POULTRY_SWINE)));
			observation.LandUse.residential = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.RESIDENTIAL)));
			observation.LandUse.stores = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.STORES)));
			observation.LandUse.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationLandUse.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationLandUse table
		mCursor = mDb.rawQuery(ObservationMaterial.SELECT_BY_ID, new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.Material.bedrock = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationMaterial.BEDROCK)));
			observation.Material.boulders = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationMaterial.BOULDERS)));
			observation.Material.gravel = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationMaterial.GRAVEL)));
			observation.Material.other = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationMaterial.OTHER)));
			observation.Material.sand = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationMaterial.SAND)));
			observation.Material.silt = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationMaterial.SILT)));
			observation.Material.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationMaterial.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationLandUse table
		mCursor = mDb.rawQuery(ObservationSmell.SELECT_BY_ID, new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.Smell.chemical = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.CHEMICAL)));
			observation.Smell.chlronine = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.CHLORINE)));
			observation.Smell.fishy = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.FISHY)));
			observation.Smell.gasolineOil = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.GASOLINE_OIL)));
			observation.Smell.noOdor = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.NO_ODOR)));
			observation.Smell.rottenEggs = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.ROTTEN_EGGS)));
			observation.Smell.rotting = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.ROTTING)));
			observation.Smell.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationSmell.OBSERVATION_ID));
		}
		mCursor.close();

		// Get info from ObservationLandUse table
		mCursor = mDb.rawQuery(ObservationStreamUse.SELECT_BY_ID, new String[] {"" + id});
		if(mCursor.moveToFirst()){
			observation.StreamUse.agriculture = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.AGRICULTURE)));
			observation.StreamUse.drinking = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.DRINKING)));
			observation.StreamUse.fishing = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.FISHING)));
			observation.StreamUse.industrial = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.INDUSTRIAL)));
			observation.StreamUse.irrigation = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.IRRIGATION)));
			observation.StreamUse.livestock = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.LIVESTOCK)));
			observation.StreamUse.recreation = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.RECREATION)));
			observation.StreamUse.swimming = intToBool(mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.SWIMMING)));
			observation.StreamUse.observationId = mCursor.getInt(mCursor.getColumnIndex(ObservationStreamUse.OBSERVATION_ID));
		}
		mCursor.close();

		return observation;
	}

	public void saveObservation(Observation observation){
		ContentValues observationValues = new ContentValues();
		observationValues.put(Observation.ANONYMOUS, observation.anonymous);
		observationValues.put(Observation.COMMENTS, observation.comments);
		observationValues.put(Observation.DATE, observation.date);
		observationValues.put(Observation.FISH_PRESENCE, observation.fishPrescence);
		observationValues.put(Observation.FISH_TYPES, observation.fishTypes);
		observationValues.put(Observation.IMAGE_URI_CAT_1, observation.imageUriCat1);
		observationValues.put(Observation.IMAGE_URI_CAT_2, observation.imageUriCat2);
		observationValues.put(Observation.IMAGE_URI_CAT_3, observation.imageUriCat3);
		observationValues.put(Observation.IMAGE_URI_CAT_4, observation.imageUriCat4);
		observationValues.put(Observation.LEFT_BANK, observation.leftBank);
		observationValues.put(Observation.LOCATION_LAT, observation.locationLat);
		observationValues.put(Observation.LOCATION_LONG, observation.locationLong);
		observationValues.put(Observation.PIPES, observation.pipes);
		observationValues.put(Observation.PIPE_SECRETION, observation.pipeSecretion);
		observationValues.put(Observation.REP_AMPH_PRESENT, observation.repAmphPresent);
		observationValues.put(Observation.RIGHT_BANK, observation.rightBank);
		observationValues.put(Observation.STATE_MUNI, observation.stateMuni);
		observationValues.put(Observation.TRASH, observation.trash);
		observationValues.put(Observation.TREE_SHADE, observation.treeSahde);
		observationValues.put(Observation.WATERBODY, observation.waterbody);
		observationValues.put(Observation.WATER_MOVEMENT, observation.waterMovement);
		observationValues.put(Observation.WATERSHED, observation.watershed);
		observationValues.put(Observation.WATER_SPEED, observation.waterSpeed);

		ContentValues observationAlgeaValues = new ContentValues();
		observationAlgeaValues.put(ObservationAlgea.ATTACHED_TO_ROCKS, observation.Algea.attachedToRocks);
		observationAlgeaValues.put(ObservationAlgea.EVERYWHERE, observation.Algea.everywhere);
		observationAlgeaValues.put(ObservationAlgea.FLOATING, observation.Algea.floating);
		observationAlgeaValues.put(ObservationAlgea.MATTED_ON_THE_STREAMBED, observation.Algea.mattedOnTheStreamBed);
		observationAlgeaValues.put(ObservationAlgea.NOT_PRESENT, observation.Algea.notPresent);
		observationAlgeaValues.put(ObservationAlgea.PRESENT_IN_SPOTS, observation.Algea.presentInSpots);

		ContentValues observationAlgeaColorValues = new ContentValues();
		observationAlgeaColorValues.put(ObservationAlgeaColor.BROWN, observation.AlgeaColor.brown);
		observationAlgeaColorValues.put(ObservationAlgeaColor.DARK_GREEN, observation.AlgeaColor.darkGreen);
		observationAlgeaColorValues.put(ObservationAlgeaColor.LIGHT_GREEN, observation.AlgeaColor.lightGreent);
		observationAlgeaColorValues.put(ObservationAlgeaColor.ORANGE, observation.AlgeaColor.orange);
		observationAlgeaColorValues.put(ObservationAlgeaColor.RED, observation.AlgeaColor.red);

		ContentValues observationBarriersValues = new ContentValues();
		observationBarriersValues.put(ObservationBarriers.BRIDGES, observation.Barriers.bridges);
		observationBarriersValues.put(ObservationBarriers.DAMS, observation.Barriers.dams);
		observationBarriersValues.put(ObservationBarriers.WATERFALLS, observation.Barriers.waterfalls);
		observationBarriersValues.put(ObservationBarriers.WOODY_DEBRIS, observation.Barriers.woodyDebris);

		ContentValues observationColorValues = new ContentValues();
		observationColorValues.put(ObservationColor.BLACK, observation.Color.balck);
		observationColorValues.put(ObservationColor.BROWN, observation.Color.brown);
		observationColorValues.put(ObservationColor.CLEAR, observation.Color.clear);
		observationColorValues.put(ObservationColor.FOAMY, observation.Color.foamy);
		observationColorValues.put(ObservationColor.GREEN, observation.Color.green);
		observationColorValues.put(ObservationColor.MUDDY, observation.Color.muddy);
		observationColorValues.put(ObservationColor.OILY, observation.Color.oily);
		observationColorValues.put(ObservationColor.OTHER, observation.Color.other);

		ContentValues observationCompositionValues = new ContentValues();
		observationCompositionValues.put(ObservationComposition.BOULDERS, observation.Composition.boulders);
		observationCompositionValues.put(ObservationComposition.GRAVEL, observation.Composition.gravel);
		observationCompositionValues.put(ObservationComposition.MAN_MADE_CEMENT, observation.Composition.manMadeCement);
		observationCompositionValues.put(ObservationComposition.OTHER, observation.Composition.other);
		observationCompositionValues.put(ObservationComposition.SAND, observation.Composition.sand);
		observationCompositionValues.put(ObservationComposition.SILT, observation.Composition.silt);
		
		ContentValues observationLandUseValues = new ContentValues();
		observationLandUseValues.put(ObservationLandUse.CONSTRUCTION, observation.LandUse.construction);
		observationLandUseValues.put(ObservationLandUse.FACTORIES, observation.LandUse.factories);
		observationLandUseValues.put(ObservationLandUse.FARMING, observation.LandUse.farming);
		observationLandUseValues.put(ObservationLandUse.FOREST, observation.LandUse.forest);
		observationLandUseValues.put(ObservationLandUse.LOGGING, observation.LandUse.logging);
		observationLandUseValues.put(ObservationLandUse.MINING, observation.LandUse.mining);
		observationLandUseValues.put(ObservationLandUse.PASTURE, observation.LandUse.pasture);
		observationLandUseValues.put(ObservationLandUse.POULTRY_SWINE, observation.LandUse.poultrySwine);
		observationLandUseValues.put(ObservationLandUse.RESIDENTIAL, observation.LandUse.residential);
		observationLandUseValues.put(ObservationLandUse.STORES, observation.LandUse.stores);
		
		ContentValues observationMaterialValues = new ContentValues();
		observationMaterialValues.put(ObservationMaterial.BEDROCK, observation.Material.bedrock);
		observationMaterialValues.put(ObservationMaterial.BOULDERS, observation.Material.boulders);
		observationMaterialValues.put(ObservationMaterial.GRAVEL, observation.Material.gravel);
		observationMaterialValues.put(ObservationMaterial.OTHER, observation.Material.other);
		observationMaterialValues.put(ObservationMaterial.SAND, observation.Material.sand);
		observationMaterialValues.put(ObservationMaterial.SILT, observation.Material.silt);
		
		ContentValues observationSmellValues = new ContentValues();
		observationSmellValues.put(ObservationSmell.CHEMICAL, observation.Smell.chemical);
		observationSmellValues.put(ObservationSmell.CHLORINE, observation.Smell.chlronine);
		observationSmellValues.put(ObservationSmell.FISHY, observation.Smell.fishy);
		observationSmellValues.put(ObservationSmell.GASOLINE_OIL, observation.Smell.gasolineOil);
		observationSmellValues.put(ObservationSmell.NO_ODOR, observation.Smell.noOdor);
		observationSmellValues.put(ObservationSmell.ROTTEN_EGGS, observation.Smell.rottenEggs);
		observationSmellValues.put(ObservationSmell.ROTTING, observation.Smell.rotting);
		observationSmellValues.put(ObservationSmell.SEWAGE, observation.Smell.sewage);
		
		ContentValues observationStreamUseValues = new ContentValues();
		observationStreamUseValues.put(ObservationStreamUse.AGRICULTURE, observation.StreamUse.agriculture);
		observationStreamUseValues.put(ObservationStreamUse.DRINKING, observation.StreamUse.drinking);
		observationStreamUseValues.put(ObservationStreamUse.FISHING, observation.StreamUse.fishing);
		observationStreamUseValues.put(ObservationStreamUse.INDUSTRIAL, observation.StreamUse.industrial);
		observationStreamUseValues.put(ObservationStreamUse.IRRIGATION, observation.StreamUse.irrigation);
		observationStreamUseValues.put(ObservationStreamUse.LIVESTOCK, observation.StreamUse.livestock);
		observationStreamUseValues.put(ObservationStreamUse.RECREATION, observation.StreamUse.recreation);
		observationStreamUseValues.put(ObservationStreamUse.SWIMMING, observation.StreamUse.swimming);
		
		if(observation.rowId == 0){
			long id = mDb.insert(Observation.TABLE_NAME, null, observationValues);
			observation.rowId = id;
			
			observationAlgeaValues.put(ObservationAlgea.OBSERVATION_ID, id);
			mDb.insert(ObservationAlgea.TABLE_NAME, null, observationAlgeaValues);
			observation.Algea.observationId = id;
			
			observationAlgeaColorValues.put(ObservationAlgeaColor.OBSERVATION_ID, id);
			mDb.insert(ObservationAlgeaColor.TABLE_NAME, null, observationAlgeaColorValues);
			observation.AlgeaColor.observationId = id;
			
			observationBarriersValues.put(ObservationBarriers.OBSERVATION_ID, id);
			mDb.insert(ObservationBarriers.TABLE_NAME, null, observationBarriersValues);
			observation.Barriers.observationId = id;
			
			observationColorValues.put(ObservationColor.OBSERVATION_ID, id);			
			mDb.insert(ObservationColor.TABLE_NAME, null, observationColorValues);
			observation.Color.observationId = id;
			
			observationCompositionValues.put(ObservationComposition.OBSERVATION_ID, id);			
			mDb.insert(ObservationComposition.TABLE_NAME, null, observationCompositionValues);
			observation.Composition.observationId = id;
			
			observationLandUseValues.put(ObservationLandUse.OBSERVATION_ID, id);
			mDb.insert(ObservationLandUse.TABLE_NAME, null, observationLandUseValues);
			observation.LandUse.observationId = id;
			
			observationMaterialValues.put(ObservationMaterial.OBSERVATION_ID, id);
			mDb.insert(ObservationMaterial.TABLE_NAME, null, observationMaterialValues);
			observation.Material.observationId = id;
			
			observationSmellValues.put(ObservationSmell.OBSERVATION_ID, id);
			mDb.insert(ObservationSmell.TABLE_NAME, null, observationSmellValues);
			observation.Smell.observationId = id;
			
			observationStreamUseValues.put(ObservationStreamUse.OBSERVATION_ID, id);
			mDb.insert(ObservationStreamUse.TABLE_NAME, null, observationStreamUseValues);
			observation.StreamUse.observationId = id;
		}
		else
		{
			mDb.update(Observation.TABLE_NAME, observationValues, Observation.ROW_ID + "=" + observation.rowId, null);
			mDb.update(ObservationAlgea.TABLE_NAME, observationAlgeaValues, ObservationAlgea.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationAlgeaColor.TABLE_NAME, observationAlgeaColorValues, ObservationAlgeaColor.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationBarriers.TABLE_NAME, observationBarriersValues, ObservationBarriers.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationColor.TABLE_NAME, observationColorValues, ObservationColor.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationComposition.TABLE_NAME, observationCompositionValues, ObservationComposition.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationLandUse.TABLE_NAME, observationLandUseValues, ObservationLandUse.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationMaterial.TABLE_NAME, observationMaterialValues, ObservationMaterial.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationSmell.TABLE_NAME, observationSmellValues, ObservationSmell.OBSERVATION_ID + "=" + observation.rowId, null);
			mDb.update(ObservationStreamUse.TABLE_NAME, observationStreamUseValues, ObservationStreamUse.OBSERVATION_ID + "=" + observation.rowId, null);
		}
	}

	private boolean intToBool(int i)
	{
		return i != 0;
	}
}
