package com.watershedapp.db;

public class ObservationLandUse {
	public static final String TABLE_NAME = "observation_land_use";
	
	// Column names
	public static final String OBSERVATION_ID = "obseravtion_id";
	public static final String FARMING = "land_farming";
	public static final String FOREST = "land_forest";
	public static final String RESIDENTIAL = "land_residential";
	public static final String POULTRY_SWINE = "land_poultry_swine";
	public static final String PASTURE = "land_pasture";
	public static final String STORES = "land_stores";
	public static final String MINING = "land_mining";
	public static final String CONSTRUCTION = "land_construction";
	public static final String FACTORIES = "land_factories";
	public static final String LOGGING = "land_logging";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
					OBSERVATION_ID + " integer primary key not null, " +
					FARMING + " integer not null, " +
					FOREST + " integer not null, " +
					RESIDENTIAL + " integer not null, " +
					POULTRY_SWINE + " integer not null, " +
					PASTURE + " integer not null, " +
					STORES + " integer not null, " +
					MINING + " integer not null, " +
					CONSTRUCTION + " integer not null, " +
					FACTORIES + " integer not null, " +
					LOGGING + " integer not null" +
				");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean farming;
	public boolean forest;
	public boolean residential;
	public boolean poultrySwine;
	public boolean pasture;
	public boolean stores;
	public boolean mining;
	public boolean construction;
	public boolean factories;
	public boolean logging;
}
