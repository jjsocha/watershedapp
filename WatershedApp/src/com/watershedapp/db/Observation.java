package com.watershedapp.db;

import android.net.Uri;

public class Observation {
	public static final String TABLE_NAME = "observation";
	
	// Column Names
	public static final String ROW_ID = "id";
	public static final String DATE = "observation_date";
	public static final String WATERBODY = "observation_waterbody";
	public static final String WATERSHED = "observation_watershed";
	public static final String STATE_MUNI = "observation_state_muni";
	public static final String LOCATION_LONG = "observation_location_long";
	public static final String LOCATION_LAT = "observation_location_lat";
	public static final String WATER_SPEED = "observation_water_speed";
	public static final String WATER_MOVEMENT= "observation_water_movement";
	public static final String FISH_PRESENCE = "observation_fish_presence";
	public static final String REP_AMPH_PRESENT = "observation_rep_amph_present";
	public static final String FISH_TYPES = "observation_fish_types";
	public static final String TREE_SHADE = "observation_tree_shade";
	public static final String LEFT_BANK = "observation_left_bank";
	public static final String RIGHT_BANK = "observation_right_bank";
	public static final String PIPES = "observation_pipes";
	public static final String PIPE_SECRETION = "observation_pipe_secretion";
	public static final String TRASH = "observation_trash";
	public static final String COMMENTS = "observation_comments";
	public static final String ANONYMOUS = "observation_anonymous";
	public static final String IMAGE_URI_CAT_1 = "observation_photo_category_1";
	public static final String IMAGE_URI_CAT_2 = "observation_photo_category_2";
	public static final String IMAGE_URI_CAT_3 = "observation_photo_category_3";
	public static final String IMAGE_URI_CAT_4 = "observation_photo_category_4";

	
	public static final String CREATE_TABLE =
			"create table " + TABLE_NAME + "(" +
				ROW_ID + " integer primary key autoincrement, " +
				DATE + " text not null, " +
				WATERBODY + " text not null, " +
				WATERSHED + " text not null, " +
				STATE_MUNI + " text not null, " +
				LOCATION_LONG + " text not null, " +
				LOCATION_LAT + " text not null, " +
				WATER_SPEED + " text not null, " +
				WATER_MOVEMENT + " text not null, " +
				FISH_PRESENCE + " text not null, " +
				REP_AMPH_PRESENT + " text not null, " +
				FISH_TYPES + " text not null, " +
				TREE_SHADE + " text not null, " +
				LEFT_BANK + " text not null, " +
				RIGHT_BANK + " text not null, " +
				PIPES + " text not null, " +
				PIPE_SECRETION + " text not null, " +
				TRASH + " text not null, " +
				COMMENTS + " text not null, " +
				ANONYMOUS + " integer not null, " +
				IMAGE_URI_CAT_1 + " null, " +
				IMAGE_URI_CAT_2 + " null, " +
				IMAGE_URI_CAT_3 + " null, " +
				IMAGE_URI_CAT_4 + " null" +
			");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + Observation.TABLE_NAME + 
			" WHERE " + ROW_ID + " = ?";
	
	public long rowId;
	public String date = "";
	public String waterbody = "";
	public String watershed = "";
	public String stateMuni = "";
	public String locationLong = "";
	public String locationLat = "";
	public String waterSpeed = "";
	public String waterMovement = "";
	public String fishPrescence = "";
	public String repAmphPresent = "";
	public String fishTypes = "";
	public String treeSahde = "";
	public String leftBank = "";
	public String rightBank = "";
	public String pipes = "";
	public String pipeSecretion = "";
	public String trash = "";
	public String comments = "";
	public boolean anonymous;
	public String imageUriCat1 = "";
	public String imageUriCat2 = "";	
	public String imageUriCat3 = "";
	public String imageUriCat4 = "";
	public ObservationAlgea Algea = new ObservationAlgea();
	public ObservationAlgeaColor AlgeaColor = new ObservationAlgeaColor();
	public ObservationBarriers Barriers = new ObservationBarriers();
	public ObservationColor Color = new ObservationColor();
	public ObservationComposition Composition = new ObservationComposition();
	public ObservationLandUse LandUse = new ObservationLandUse();
	public ObservationMaterial Material = new ObservationMaterial();
	public ObservationSmell Smell = new ObservationSmell();
	public ObservationStreamUse StreamUse = new ObservationStreamUse();
	
	@Override
	public String toString()
	{
		return waterbody + " - " + watershed;
	}
	
}
