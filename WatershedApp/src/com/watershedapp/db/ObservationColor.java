package com.watershedapp.db;

public class ObservationColor {
	public static final String TABLE_NAME = "observation_color";
	
	// Column names
	public static final String OBSERVATION_ID = "observation_id";
	public static final String CLEAR = "color_clear";
	public static final String OILY = "color_oily";
	public static final String BLACK = "color_black";
	public static final String FOAMY = "color_foamy";
	public static final String GREEN = "color_green";
	public static final String MUDDY = "color_muddy";
	public static final String BROWN = "color_brown";
	public static final String OTHER = "color_other";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
				OBSERVATION_ID + " integer primary key, " +
				CLEAR + " integer not null, " +
				OILY + " integer not null, " +
				BLACK + " integer not null, " +
				FOAMY + " integer not null, " +
				GREEN + " integer not null, " +
				MUDDY + " integer not null, " +
				BROWN + " integer not null, " +
				OTHER + " integer not null" +
			");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean clear;
	public boolean oily;
	public boolean balck;
	public boolean foamy;
	public boolean green;
	public boolean muddy;
	public boolean brown;
	public boolean other;
}
