package com.watershedapp.db;

public class ObservationBarriers {
	public static final String TABLE_NAME = "observation_barriers";
	
	// Column names
	public static final String OBSERVATION_ID = "observation_id";
	public static final String DAMS = "barriers_dams";
	public static final String BRIDGES = "barriers_bridges";
	public static final String WOODY_DEBRIS = "barriers_woody_debris";
	public static final String WATERFALLS = "barriers_waterfalls";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
					OBSERVATION_ID + " integer primary key, " +
					DAMS + " integer not null, " +
					BRIDGES + " integer not null, " +
					WOODY_DEBRIS + " integer not null, " +
					WATERFALLS + " integer not null" +
				");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean dams;
	public boolean bridges;
	public boolean woodyDebris;
	public boolean waterfalls;
}
