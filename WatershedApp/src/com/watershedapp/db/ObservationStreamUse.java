package com.watershedapp.db;

public class ObservationStreamUse {
	public static final String TABLE_NAME = "observation_stream_use";

	// Column names
	public static final String OBSERVATION_ID = "observation_id";
	public static final String DRINKING = "stream_drinking";
	public static final String RECREATION = "stream_recreation";
	public static final String SWIMMING = "stream_swimming";
	public static final String FISHING = "stream_fishing";
	public static final String INDUSTRIAL = "stream_industrial";
	public static final String AGRICULTURE = "stream_agriculture";
	public static final String IRRIGATION = "stream_irrigation";
	public static final String LIVESTOCK = "stream_livestock";
	
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
				OBSERVATION_ID + " integer primary key, " +
				DRINKING + " integer not null, " +
				RECREATION + " integer not null, " +
				SWIMMING + " integer not null, " +
				FISHING + " integer not null, " +
				INDUSTRIAL + " integer not null, " +
				AGRICULTURE + " integer not null, " +
				IRRIGATION + " integer not null, " +
				LIVESTOCK + " integer not null" +
			");";
	public static final String SELECT_BY_ID = "SELECT * FROM " + TABLE_NAME + 
			" WHERE " + OBSERVATION_ID  + " = ?";
	
	public long observationId;
	public boolean drinking;
	public boolean recreation;
	public boolean swimming;
	public boolean fishing;
	public boolean industrial;
	public boolean agriculture;
	public boolean irrigation;
	public boolean livestock;
}
