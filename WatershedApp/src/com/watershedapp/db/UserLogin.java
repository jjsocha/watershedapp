package com.watershedapp.db;

public class UserLogin {
	public static final String TABLE_NAME = "UserLogin";
	
	// Column names
	public static final String ROWID = "_id";
	public static final String USERNAME = "UserName";
	public static final String LOGINTOKEN = "LoginToken";
	public static final String CREATE_TABLE = 
			"create table " + TABLE_NAME + "(" +
				ROWID + " integer primary key autoincrement, " +
				USERNAME + " text not null, " + 
				LOGINTOKEN+" text not null" +
			");";
	
	public long Id = -1;
	public String Username;
	public String Token;
}
