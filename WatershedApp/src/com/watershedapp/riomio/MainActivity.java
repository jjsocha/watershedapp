package com.watershedapp.riomio;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.example.watershedapp.R;
import com.watershedapp.db.UserLogin;
import com.watershedapp.db.WatershedDbAdapter;
import com.watershedapp.riomio.DisplayMainActivity;

public class MainActivity extends Activity {
	public final static String EXTRA_MESSAGE = "com.example.myfirstapp.MESSAGE";
	public final static String TAG = "MainActivity";
	private EditText username;
	private EditText password;
	private WatershedDbAdapter dbAdapter;
	private UserLogin userLogin;
	FrameLayout mFrame;
	View tutView; 
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mFrame = new FrameLayout(this);
		getLayoutInflater().inflate(R.layout.activity_main, mFrame);

		tutView = LayoutInflater.from(getBaseContext()).inflate(R.layout.scripts_tut, null);
		//mFrame.addView(tutView);
		setContentView(mFrame);
		Intent i = getIntent();
		String logout = i.getStringExtra("logout");

		username = (EditText)findViewById(R.id.username);
		password = (EditText)findViewById(R.id.password);
		dbAdapter = new WatershedDbAdapter(MainActivity.this);
		userLogin = dbAdapter.open().getCurrentUser();

		if(userLogin != null){
			if(logout != null && logout.equals("logout")){
				dbAdapter.delteAllUsers();
			}else if(logout != null){
				username.setText(userLogin.Username);
			}
			else{
				Intent k = new Intent(this, DisplayMainActivity.class);
				startActivity(k);
				finish();
				return;
			}
		}
		
		if(android.os.Build.VERSION.SDK_INT > 10)
		{
			ActionBar mActionBar = getActionBar();
			mActionBar.setBackgroundDrawable(new ColorDrawable(Color.rgb(34, 129, 191)));
		}
	}

	public void removeView(View v){
		Log.d("AZAZ", tutView.toString());
		mFrame.removeView(tutView);
	}
	
	@Override
	public void onBackPressed() {
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	/** Called when the user clicks the Send button */
	public void sendMessage(View view) {
		// Do something in response to button
		Intent intent = new Intent(this, DisplayMainActivity.class);

		EditText editText = (EditText) findViewById(R.id.action_help);
		String message = editText.getText().toString();
		intent.putExtra(EXTRA_MESSAGE, message);
		startActivity(intent);
	}

	public void login(View v)
	{
		dbAdapter.open().delteAllUsers();
		long test = dbAdapter.addUser(username.getText().toString(),
				password.getText().toString());
		dbAdapter.close();
		Log.d(TAG, "Login button was clicked user login id is " + test);
		Intent k = new Intent(this, DisplayMainActivity.class);
		startActivity(k);
		finish();
	} 

	public void skip(View v)
	{
		Log.d(TAG, "Skip button was clicked");
		Intent k = new Intent(this, DisplayMainActivity.class);
		startActivity(k);
		finish();
	}

	public boolean onMenuItemSelected(int id, MenuItem item){
		Log.d("BLAH", id +"");
		switch(item.getItemId()){

		case R.id.action_help:
			mFrame.addView(tutView);
			return true;
		}
		return false;
	}
}
