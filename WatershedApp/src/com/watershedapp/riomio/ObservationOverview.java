package com.watershedapp.riomio;

import java.util.ArrayList;

import com.example.watershedapp.R;
import com.watershedapp.db.Observation;
import com.watershedapp.db.WatershedDbAdapter;

import android.os.Bundle;
import android.app.ActionBar;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.support.v4.app.NavUtils;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.graphics.PorterDuff;

public class ObservationOverview extends Activity {
	View helpView; 
	FrameLayout mFrame;
	boolean categories[][];
	int[] missingQuestionIDs;
	long obs_id;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		  mFrame = new FrameLayout(this);
		  getLayoutInflater().inflate(R.layout.activity_observation_overview, mFrame);
		  
		  helpView = LayoutInflater.from(getBaseContext()).inflate(R.layout.overviewhelp, null);
	        //mFrame.addView(tutView);
	        setContentView(mFrame);
	        

		//helpView = LayoutInflater.from(getBaseContext()).inflate(R.layout.scripts_tut, null);
		// Show the Up button in the action bar.
		setupActionBar();
		
		Intent overview = getIntent();
		obs_id = overview.getLongExtra("id", -1);
		
		if (obs_id == -1)
		{
			Log.d("LoadObs", "Uh oh, didn't get observation!");
			return;
		}
		
		//See findMissingData() for details of these
		categories = new boolean[6][];  //5 Categories of Questions + 1 Summary Array
		categories[0] = new boolean[6];  //7 Questions in cat 1
		categories[1] = new boolean[5];  //6 Questions in cat 2
		categories[2] = new boolean[8];  //...
		categories[3] = new boolean[6];
		categories[4] = new boolean[2];
		categories[5] = new boolean[5];  //5 Categories to summarize
		
		findMissingData();
		
		Button sectionJumps[] = new Button[5];
		sectionJumps[0] = ((Button) findViewById(R.id.button1));
		sectionJumps[1] = ((Button) findViewById(R.id.button2));
		sectionJumps[2] = ((Button) findViewById(R.id.button3));
		sectionJumps[3] = ((Button) findViewById(R.id.button4));
		sectionJumps[4] = ((Button) findViewById(R.id.button5));
		
		for (int i=0; i<sectionJumps.length; i++)
		{
			if (categories[5][i] == true)
			{
				sectionJumps[i].getBackground().setColorFilter(Color.RED,PorterDuff.Mode.MULTIPLY);   //Full opacity red  (0xAABBCCDD) => ((AA = Alpha), (BB = Red), (CC = Green), (DD = Blue))
				sectionJumps[i].setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.no,0);
			}
			else
			{
				sectionJumps[i].getBackground().setColorFilter(Color.GREEN,PorterDuff.Mode.MULTIPLY);   //Full opacity green
				sectionJumps[i].setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.yes,0);
			}
		}
		
		if(android.os.Build.VERSION.SDK_INT > 10)
		{
			ActionBar mActionBar = getActionBar();
			mActionBar.setBackgroundDrawable(new ColorDrawable(Color.rgb(34, 129, 191)));
		}
		
	}

	private void findMissingData() {
		
		//New way thats betterer
		WatershedDbAdapter db = new WatershedDbAdapter(this);
		db.open();
		
		Observation answers = db.getObservation(obs_id);
		
		db.close();
		
		//....use answers to basically do what the below does
		
		//Crappy old way of doing this, remove later
		
		//categories is arranged as such (jagged array):
		//  (nX is number of questions in category X)
		//
		//  { Category 1 Question 1 Unanswered, Category 1 Question 2 Unanswered, ...., Category 1 Question n1 Unanswered}
		//  { Category 2 Question 1 Unanswered, Category 2 Question 2 Unanswered, ...., Category 2 Question n2 Unanswered}
		//  { ..... }
		//  { ..... }
		//  { ..... }
		//  { Category 1 At Least 1 Question Unanswered, Category 2 At Least 1 Question Unanswered, ...., Category 5 At Least 1 Question Unanswered}
		
		findMissingCategoryOne(answers);
		findMissingCategoryTwo(answers);
		findMissingCategoryThree(answers);
		findMissingCategoryFour(answers);
		findMissingCategoryFive(answers);
		
		ArrayList<Integer> temp = assignMissingQuestions();
		
		missingQuestionIDs = new int[temp.size()];
		
		for (int i=0; i<missingQuestionIDs.length; i++)
		{
			missingQuestionIDs[i] = temp.get(i);
		}
		
	}
	
	private ArrayList<Integer> assignMissingQuestions() {
		ArrayList<Integer> ids = new ArrayList<Integer>();
		int QIDs[][] = {{R.id.lblDate, R.id.lblWaterbody, R.id.lblWatershedName, R.id.lblState, R.id.lblLongitude, R.id.lblLatitude, R.id.photo1Cat1},
						{R.id.lblWaterColor, R.id.lblSmell, R.id.lblSpeed, R.id.lblStreamwalk, R.id.lblComposition},
						{R.id.lblAlgaePresence, R.id.lblAlgaeColor, R.id.lblFishPresence, R.id.lblFishDiversity, R.id.lblOtherAnimalLife, R.id.lblShadeCoverage, R.id.lblLeftBankDetails, R.id.lblRightBankDetails},
						{R.id.lblLandUse, R.id.lblStreamUse, R.id.lblPipes, R.id.lblFullPipes, R.id.lblTrash, R.id.lblBarriers},
						{R.id.lblAnonymous, R.id.lblComments}
					   };
		
		for (int i=0; i<categories.length-1; i++)
		{
			for (int j=0; j<categories[i].length; j++)
			{
				if (categories[i][j])
				{
					ids.add(new Integer(QIDs[i][j]));
				}
			}
		}
		
		return ids;
	}

	private void findMissingTextQuestions(String[] ans, int[] indecies, int categoryIndex)
	{
		for (int i=0; i<ans.length; i++)
		{
			ans[i] = (ans[i] == null) ? null : ans[i].trim();
			
			//If no answer or answer is blank, treat as missing data
			if (ans[i] == null || ans[i].length() == 0)
			{
				categories[categoryIndex][indecies[i]] = true;
				categories[5][categoryIndex] = true;
			}
			//Otherwise text is present, answer not missing
			else
			{
				categories[categoryIndex][indecies[i]] = false;
			}
		}
	}
	
	private void findMissingCheckboxQuestions(boolean[][] ans, int[] indecies, int categoryIndex)
	{
		for (int i=0; i<ans.length; i++)
		{
			//Assume answer is missing for this question until proven otherwise
			categories[categoryIndex][indecies[i]] = true;
			for (int j=0; j<ans[i].length; j++)
			{
				if (ans[i][j] == true)
				{
					//Once I find a checked box, answer is not missing
					categories[categoryIndex][indecies[i]] = false;
					break;
				}
			}
			
			//If answer is missing for this question
			if (categories[categoryIndex][indecies[i]] == true)
			{
				//Category is missing data
				categories[5][categoryIndex] = true;
			}
		}
	}
	
	private void findMissingCategoryFive(Observation ans) {		
		int indecies[] = {1};
		String ansText[] = {ans.comments};
		
		int categoryIndex = 4;
		
		categories[5][categoryIndex] = false;
		
		//Check radio button & free text questions
		findMissingTextQuestions(ansText, indecies, categoryIndex);
		
	}
	
	private void findMissingCategoryFour(Observation ans) {
		int indeciesChk[] = {0, 1, 5};
		
		//***** NOTE **** 
		//I notice "pasture" is an option for landuse, but when I made the questionnaire, that wasn't in the list, so this doesn't check for it
		boolean ansChk[][] = {{ans.LandUse.construction, ans.LandUse.factories, ans.LandUse.farming, ans.LandUse.forest, ans.LandUse.logging, ans.LandUse.mining, ans.LandUse.poultrySwine, ans.LandUse.residential, ans.LandUse.stores},
							  {ans.StreamUse.agriculture, ans.StreamUse.drinking, ans.StreamUse.fishing, ans.StreamUse.industrial, ans.StreamUse.irrigation, ans.StreamUse.livestock, ans.StreamUse.recreation, ans.StreamUse.swimming},
							  {ans.Barriers.bridges, ans.Barriers.dams, ans.Barriers.waterfalls, ans.Barriers.woodyDebris}
							 };
		
		int indeciesRdo[] = {2, 3, 4};
		String ansRdo[] = {ans.pipes, ans.pipeSecretion, ans.trash};
		
		int categoryIndex = 3;
		
		categories[5][categoryIndex] = false;
		
		//Check checkbox questions
		findMissingCheckboxQuestions(ansChk, indeciesChk, categoryIndex);
		
		//Check radio button questions
		findMissingTextQuestions(ansRdo, indeciesRdo, categoryIndex);
	}
	
	private void findMissingCategoryThree(Observation ans) {
		int indeciesChk[] = {0, 1};
		boolean ansChk[][] = {{ans.Algea.attachedToRocks, ans.Algea.everywhere, ans.Algea.floating, ans.Algea.mattedOnTheStreamBed, ans.Algea.notPresent, ans.Algea.presentInSpots},
							  {ans.AlgeaColor.brown, ans.AlgeaColor.darkGreen, ans.AlgeaColor.lightGreent, ans.AlgeaColor.orange, ans.AlgeaColor.red}
							 };
		
		
		int indeciesRdo[] = {2, 3, 4, 5, 6, 7};
		String ansRdo[] = {ans.fishPrescence, ans.fishTypes, ans.repAmphPresent, ans.treeSahde, ans.leftBank, ans.rightBank};
		
		int categoryIndex = 2;
		
		categories[5][categoryIndex] = false;
		
		//Check checkbox questions
		findMissingCheckboxQuestions(ansChk, indeciesChk, categoryIndex);
		
		//Check radio button questions
		findMissingTextQuestions(ansRdo, indeciesRdo, categoryIndex);
		
	}
	
	private void findMissingCategoryTwo(Observation ans) {
		int indeciesChk[] = {0, 1, 4};
		boolean ansChk[][] = {{ans.Color.balck, ans.Color.brown, ans.Color.clear, ans.Color.foamy, ans.Color.green, ans.Color.muddy, ans.Color.oily, ans.Color.other},
							  {ans.Smell.chemical, ans.Smell.chlronine, ans.Smell.fishy, ans.Smell.gasolineOil, ans.Smell.noOdor, ans.Smell.rottenEggs, ans.Smell.rotting, ans.Smell.sewage},
							  {ans.Composition.boulders, ans.Composition.gravel, ans.Composition.manMadeCement, ans.Composition.other, ans.Composition.sand, ans.Composition.silt}
							 };
		
		
		int indeciesRdo[] = {2, 3};
		String ansRdo[] = {ans.waterSpeed, ans.waterMovement};
		
		int categoryIndex = 1;
		
		categories[5][categoryIndex] = false;
		
		//Check checkbox questions
		findMissingCheckboxQuestions(ansChk, indeciesChk, categoryIndex);
		
		//Check radio button questions
		findMissingTextQuestions(ansRdo, indeciesRdo, categoryIndex);
		
	}
	
	private void findMissingCategoryOne(Observation ans) {
		String answers[] = {ans.date, ans.waterbody, ans.watershed, ans.stateMuni, ans.locationLong, ans.locationLat};
		int indecies[] = {0, 1, 2, 3, 4, 5};
		
		int categoryIndex = 0;
		
		categories[0][categoryIndex] = false;

		//Check free text questions
		findMissingTextQuestions(answers, indecies, categoryIndex);
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.observation_overview, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	} 
	
	public void observationDetails(View v)
	{
		Intent k = new Intent(this, Questionnaire.class);
		k.putExtra("section", 1);
		k.putExtra("id", obs_id);
		k.putExtra("missing", missingQuestionIDs);
    	startActivity(k);
    	finish();
	}
	
	public void streamDetails(View v)
	{
		Intent k = new Intent(this, Questionnaire.class);
		k.putExtra("section", 2);
		k.putExtra("id", obs_id);
		k.putExtra("missing", missingQuestionIDs);
    	startActivity(k);
    	finish();
	}
	
	public void floraFaunaDetails(View v)
	{
		Intent k = new Intent(this, Questionnaire.class);
		k.putExtra("section", 3);
		k.putExtra("id", obs_id);
		k.putExtra("missing", missingQuestionIDs);
    	startActivity(k);
    	finish();
	}
	
	public void landUseDetails(View v)
	{
		Intent k = new Intent(this, Questionnaire.class);
		k.putExtra("section", 4);
		k.putExtra("id", obs_id);
		k.putExtra("missing", missingQuestionIDs);
		startActivity(k);
		finish();
	}
	
	public void miscellaneous(View v)
	{
		Intent k = new Intent(this, Questionnaire.class);
		k.putExtra("section", 5);
		k.putExtra("id", obs_id);
		k.putExtra("missing", missingQuestionIDs);
    	startActivity(k);
    	finish();
	}
	
	//TODO: make both of these move to the main page not login
	public void submit(View v)
	{
		WatershedDbAdapter db = new WatershedDbAdapter(this).open();
		db.deleteObservation(obs_id);
		db.close();
		Intent k = new Intent(this, MainActivity.class);
    	startActivity(k);
    	finish();
	}
	public void saveForLater(View v)
	{
		Intent k = new Intent(this, MainActivity.class);
    	startActivity(k);
    	finish();
	}
	public void removeView(View v){
		mFrame.removeView(helpView);
	}
	public boolean onMenuItemSelected(int id, MenuItem item){
    
    	switch(item.getItemId()){

    	case R.id.help:
    		mFrame.addView(helpView);
    		return true;
    	}
    	return false;
    }
}
