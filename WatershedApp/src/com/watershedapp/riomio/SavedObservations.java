package com.watershedapp.riomio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.example.watershedapp.R;
import com.watershedapp.db.Observation;
import com.watershedapp.db.WatershedDbAdapter;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

public class SavedObservations extends Activity{
	FrameLayout mFrame;
	StableArrayAdapter adapter;
	static final String TAG = "SavedObservations";
	final ArrayList<Observation> list = new ArrayList<Observation>();

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_saved_observations);

		if(android.os.Build.VERSION.SDK_INT > 10)
		{
			ActionBar ab = getActionBar(); 
			ab.setDisplayHomeAsUpEnabled(true);
			ab.setBackgroundDrawable(new ColorDrawable(Color.rgb(34, 129, 191)));
		}

		final ListView listview = (ListView) findViewById(R.id.observationList);
		WatershedDbAdapter db = new WatershedDbAdapter(this);
		for(Observation obs : db.open().getAllObservations()){
			list.add(obs);
		}

		adapter = new StableArrayAdapter(this,
				android.R.layout.simple_list_item_1, list);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(final AdapterView<?> parent, final View view,
					int position, long id) {
				Log.d(TAG, "item clicked");
				Intent i = new Intent(SavedObservations.this, Questionnaire.class);
				i.putExtra("id", ((Observation)parent.getItemAtPosition(position)).rowId);
				startActivity(i);
				SavedObservations.this.finish();
			}
		});
		listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(final AdapterView<?> parent, final View view,
					int position, long id) {
				Log.d(TAG, "item long clicked");
				final Observation observation = (Observation)parent.getItemAtPosition(position);
				Builder alert = new AlertDialog.Builder(SavedObservations.this);
				alert.setTitle(getString(R.string.delete_observation));
				alert.setMessage(getString(R.string.delete_question) + " " + observation + "?");
				alert.setPositiveButton("Yes", new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						WatershedDbAdapter db = new WatershedDbAdapter(SavedObservations.this).open();
						db.deleteObservation(observation.rowId);
						db.close();
						list.remove(observation);
						adapter.notifyDataSetChanged();
					}
				});
				alert.setNegativeButton("No", null);
				alert.show();
				return true;
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent i = new Intent(this, DisplayMainActivity.class);
			startActivity(i);
			finish();
			return(true);
		}
		return(super.onOptionsItemSelected(item));
	}

	@Override
	public void onBackPressed() {
		Intent i = new Intent(this, DisplayMainActivity.class);
		startActivity(i);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private class StableArrayAdapter extends ArrayAdapter<Observation> {

		HashMap<Observation, Integer> mIdMap = new HashMap<Observation, Integer>();

		public StableArrayAdapter(Context context, int textViewResourceId,
				List<Observation> objects) {
			super(context, textViewResourceId, objects);
			for (int i = 0; i < objects.size(); ++i) {
				mIdMap.put(objects.get(i), i);
			}
		}

		@Override
		public long getItemId(int position) {
			Observation item = getItem(position);
			return mIdMap.get(item);
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}
}
