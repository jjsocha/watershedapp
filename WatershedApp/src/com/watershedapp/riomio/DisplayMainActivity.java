package com.watershedapp.riomio;

import com.example.watershedapp.R;
import com.watershedapp.riomio.MainActivity;

import android.os.Build;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

public class DisplayMainActivity extends Activity {
	FrameLayout mFrame;
	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
		mFrame = new FrameLayout(this);
		getLayoutInflater().inflate(R.layout.activity_display_main, mFrame);
		setContentView(mFrame);
		
		if(android.os.Build.VERSION.SDK_INT > 10)
		{
			ActionBar mActionBar = getActionBar();
			mActionBar.setBackgroundDrawable(new ColorDrawable(Color.rgb(34, 129, 191)));
		}
	}
	
	public void createObservation(View v){
		Intent k = new Intent(this, Questionnaire.class);
		startActivity(k);
		finish();
	}
	
	public void savedObservations(View v){
		Intent k = new Intent(this, SavedObservations.class);
		startActivity(k);
		finish();
	}
	
	public boolean onMenuItemSelected(int id, MenuItem item){
		Log.d("BLAH", id +"");
		switch(item.getItemId()){
		case R.id.action_logout:
			Intent i = new Intent(this, MainActivity.class);
			i.putExtra("logout", "logout");
			startActivity(i);
			finish();
			return true;
		}
		return false;
	}
	
	@Override
	public void onBackPressed() {
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}