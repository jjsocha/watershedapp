package com.watershedapp.riomio;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.example.watershedapp.R;
import com.example.watershedapp.R.string;
import com.watershedapp.db.Observation;
import com.watershedapp.db.WatershedDbAdapter;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup; 
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Toast;

public class Questionnaire extends Activity implements LocationListener{
	static int currentSection = -1;
	long observationId;
	int missingIDs[];
	View tutView; 
	LocationManager locationManager;
	LocationListener mListener;

	int pictureStatus = 0;

	private static final String TAG = "Questionnaire";
	static final int ACTION_TAKE_PHOTO_B = 1;
	private static final String JPEG_FILE_PREFIX = "IMG_";
	private static final String JPEG_FILE_SUFFIX = ".jpg";

	String pictureTimeStamp = "";

	Uri contentUri = null;
	String photoUri1 = "";
	String photoUri2 = "";
	String photoUri3 = "";
	String photoUri4 = "";

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_questionnaire);
		tutView = LayoutInflater.from(getBaseContext()).inflate(R.layout.scripts_tut, null);

		Intent intent = getIntent();

		if(intent != null)
		{
			if(currentSection == -1){
				currentSection = intent.getIntExtra("section", 1);
			}
			showView(currentSection);
			observationId = intent.getLongExtra("id", 0);
			if(observationId != 0){
				((RelativeLayout)findViewById(R.id.goToOverview)).setVisibility(View.VISIBLE);
			}
			LOAD_ALL_THE_THINGSSS(observationId);

			missingIDs = intent.getIntArrayExtra("missing");

			if (missingIDs != null)
			{
				highlightUnanswered();
			}
		}
		if(android.os.Build.VERSION.SDK_INT > 10)
		{
			ActionBar mActionBar = getActionBar();
			mActionBar.setBackgroundDrawable(new ColorDrawable(Color.rgb(34, 129, 191)));
		}

		Button picBtn = null;


		switch(currentSection){
		case 1: picBtn = (Button) findViewById(R.id.photo1Cat1);
		break;
		case 2: picBtn = (Button) findViewById(R.id.photo1Cat2); 
		break;
		case 3: picBtn = (Button) findViewById(R.id.photo1Cat3);
		break;
		case 4: picBtn = (Button) findViewById(R.id.photo1Cat4);
		break;
		}

		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

	}

	Button.OnClickListener mTakePicOnClickListener = 
			new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (pictureStatus == 0)
				dispatchTakePictureIntent(ACTION_TAKE_PHOTO_B);
			else
				fullScreenPhoto();
		}
	};

	Button.OnLongClickListener mDeletePicOnLongClickListener =
			new Button.OnLongClickListener() {

		@Override
		public boolean onLongClick(View v) {
			if (pictureStatus == 1) {
				Toast toast = Toast.makeText(getApplicationContext(), "Deleting Picture...", Toast.LENGTH_LONG);
				removePictureURI(observationId, currentSection);
				toast.show();
			}

			else
				return false;

			return true;
		}

	};

	private void setBtnListenerOrDisable( 
			Button btn, 
			Button.OnClickListener onClickListener,
			Button.OnLongClickListener onLongClickListener,
			String intentName
			) {
		if (isIntentAvailable(this, intentName)) {
			btn.setOnClickListener(onClickListener);
			btn.setOnLongClickListener(onLongClickListener);
		} else {
			btn.setText( 
					getText(R.string.cannot).toString() + " " + btn.getText());
			btn.setClickable(false);
		}

	} // end setBtnListenerOrDisable()

	private void fullScreenPhoto() {
		WatershedDbAdapter db = new WatershedDbAdapter(this);
		db.open();

		Log.d(TAG, "URIRUIRURIIUR = " + contentUri.toString());

		FullScreenOnClick(null);
		db.close();
	}

	@SuppressLint("NewApi")
	private void dispatchTakePictureIntent(int actionCode) {
		Toast toast = Toast.makeText(getApplicationContext(), "Attempting to open camera...", Toast.LENGTH_LONG);
		toast.show();

		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File f = null;
		try {
			f = createImageFile();
			Log.d(TAG,"dispatchTakePictureIntent: f="+f);
			takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
		} catch (IOException e) {
			e.printStackTrace();
			f = null;
		}

		if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			startActivityForResult(takePictureIntent, actionCode);
		}

		galleryAddPic(f);


	} // end dispatchTakePictureIntent()

	private void galleryAddPic(File f) {
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		contentUri = Uri.fromFile(f);
		switch(currentSection){
		case 1: photoUri1 = contentUri.toString();
		break;
		case 2: photoUri2 = contentUri.toString(); 
		break;
		case 3: photoUri3 = contentUri.toString();
		break;
		case 4: photoUri4 = contentUri.toString();
		break;
		}
		mediaScanIntent.setData(contentUri);
		this.sendBroadcast(mediaScanIntent);
	}

	private File createImageFile() throws IOException {
		// Create an image file name
		pictureTimeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
		String imageFileName = JPEG_FILE_PREFIX + pictureTimeStamp + "_";
		File albumF = getAlbumDir();
		File imageF = File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF);
		return imageF;
	}

	private File getAlbumDir() {
		File storageDir = null;
		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			storageDir = new File(Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_PICTURES), getString(R.string.album_name));
			if (storageDir != null) {
				if (! storageDir.mkdirs()) {
					if (! storageDir.exists()){
						Log.d(TAG, "failed to create directory CameraSample");
						return null;
					}
				}
			}	
		} else {
			Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
		}
		return storageDir;
	} // end getAlbumDir()

	/**
	 * Indicates whether the specified action can be used as an intent. This
	 * method queries the package manager for installed packages that can
	 * respond to an intent with the specified action. If no suitable package is
	 * found, this method returns false.
	 * http://android-developers.blogspot.com/2009/01/can-i-use-this-intent.html
	 *
	 * @param context The application's environment.
	 * @param action The Intent action to check for availability.
	 *
	 * @return True if an Intent with the specified action can be sent and
	 *         responded to, false otherwise.
	 */
	public static boolean isIntentAvailable(Context context, String action) {
		final PackageManager packageManager = context.getPackageManager();
		final Intent intent = new Intent(action);
		List<ResolveInfo> list =
				packageManager.queryIntentActivities(intent,
						PackageManager.MATCH_DEFAULT_ONLY);
		return list.size() > 0;
	}

	/*
	 * Return from the Camera Activity
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG,"onActivityResult: data="+data);
		if (requestCode == ACTION_TAKE_PHOTO_B) {
			if (resultCode == RESULT_OK) {
				Log.d(TAG,"onActivityResult: ACTION_TAKE_PHOTO_B");
				try {
					Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentUri);
					Button button = null;
					switch(currentSection){
					case 1: button = (Button) findViewById(R.id.photo1Cat1);
					break;
					case 2: button = (Button) findViewById(R.id.photo1Cat2); 
					break;
					case 3: button = (Button) findViewById(R.id.photo1Cat3);
					break;
					case 4: button = (Button) findViewById(R.id.photo1Cat4);
					break;
					}

					bitmap = Bitmap.createScaledBitmap(bitmap, 66, 48, false);
					BitmapDrawable bitDraw = new BitmapDrawable(getResources(), bitmap);
					button.setHeight(48);
					button.setWidth(66);
					button.setText("");
					button.setBackground(bitDraw);
					pictureStatus = 1;



					switch(currentSection){
					case 1: button = (Button) findViewById(R.id.photo1Cat1);
					break;
					case 2: button = (Button) findViewById(R.id.photo1Cat2); 
					break;
					case 3: button = (Button) findViewById(R.id.photo1Cat3);
					break;
					case 4: button = (Button) findViewById(R.id.photo1Cat4);
					break;
					}

					Log.d(TAG,"onActivityResult: Image Bitmap = " + bitmap);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				handleBigCameraPhoto();

			}
		} // ACTION_TAKE_PHOTO_B
	} // end onActivityResult()

	private void handleBigCameraPhoto() {
		Log.d(TAG,"handleBigCameraPhoto");
		/*
		 * RLP: should check for file size and
		 * if small, should delete.
		 */
	} // end handleBigCameraPhoto()

	private void highlightUnanswered() {
		for (int i=0; i<missingIDs.length; i++)
		{
			((TextView) findViewById(missingIDs[i])).setTextColor(0xFFFF0000);
		}

	}
	public void getGPS(View v){
		Toast toast = Toast.makeText(getApplicationContext(), "Attempting to get GPS Signal", Toast.LENGTH_LONG);
		toast.show();
		locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

		mListener = this;

		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1, 0.0f, mListener);

	}


	private void removePictureURI(long id, int section)
	{
		WatershedDbAdapter db = new WatershedDbAdapter(this);

		if (section == 1) {
			RelativeLayout cat1 = (RelativeLayout) findViewById(R.id.category1);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			fs.setVisibility(View.GONE);
			cat1.setVisibility(View.VISIBLE);

		} else if (section == 2) {
			RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			fs.setVisibility(View.GONE);
			cat2.setVisibility(View.VISIBLE);

		} else if (section == 3) {
			RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			fs.setVisibility(View.GONE);
			cat3.setVisibility(View.VISIBLE);

		} else if (section == 4) {
			RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			fs.setVisibility(View.GONE);
			cat4.setVisibility(View.VISIBLE);

		}

		Button button = null;
		switch(currentSection){
		case 1: button = (Button) findViewById(R.id.photo1Cat1);
		break;
		case 2: button = (Button) findViewById(R.id.photo1Cat2); 
		break;
		case 3: button = (Button) findViewById(R.id.photo1Cat3);
		break;
		case 4: button = (Button) findViewById(R.id.photo1Cat4);
		break;
		}

		button.setSingleLine();
		button.setHeight(LayoutParams.WRAP_CONTENT);
		button.setWidth(173);
		button.setBackgroundResource(android.R.drawable.btn_default);
		button.setText(R.string.photo);

		db.open();

		switch(section){
		case 1: db.deleteImage(photoUri1, 1, id);
		break;
		case 2: db.deleteImage(photoUri2, 2, id); 
		break;
		case 3: db.deleteImage(photoUri3, 3, id);
		break;
		case 4: db.deleteImage(photoUri4, 4, id);
		break;
		}

		db.close();

		contentUri = null;
		switch(section){
		case 1: photoUri1 = "";
		break;
		case 2: photoUri2 = ""; 
		break;
		case 3: photoUri3 = "";
		break;
		case 4: photoUri4 = "";
		break;
		}
		pictureStatus = 0;

	}

	private void LOAD_ALL_THE_THINGSSS(long id) 
	{
		if (id == -1)
		{
			return;
		}

		WatershedDbAdapter db = new WatershedDbAdapter(this);
		db.open();

		Observation answers = db.getObservation(id);

		loadCategoryOne(answers);
		loadCategoryTwo(answers);
		loadCategoryThree(answers);
		loadCategoryFour(answers);
		loadCategoryFive(answers);

		db.close();

	}

	private void loadCheckBoxAnswers(int[][] ids, boolean[][] values)
	{
		CheckBox box; 
		for (int i=0; i<ids.length; i++)
		{
			for (int j=0; j<ids[i].length; j++)
			{
				box = (CheckBox) findViewById(ids[i][j]);
				box.setChecked(values[i][j]);
			}
		}
	}

	private void loadRadioButtonAnswers(int[][] ids, String[] values)
	{
		RadioButton btn;

		for (int i=0; i<ids.length; i++)
		{
			for (int j=0; j<ids[i].length; j++)
			{
				btn = (RadioButton) findViewById(ids[i][j]);
				if (btn.getText().toString().equalsIgnoreCase(values[i]))
				{
					btn.performClick();
					break;
				}
			}
		}
	}

	private void loadCategoryFive(Observation answers) {
		RadioButton anon;
		if (answers.anonymous)
		{
			anon = (RadioButton) findViewById(R.id.rdoC5Q1R_Yes);
		}
		else
		{
			anon = (RadioButton) findViewById(R.id.rdoC5Q1R_No);
		}

		anon.performClick();

		EditText comments = (EditText) findViewById(R.id.comments);

		comments.setText(answers.comments);

	}

	private void loadCategoryFour(Observation answers) {

		//Q1 Array Order: {Farming/Crops, Forest, Residential, Poultry/Swine, Stores/Malls, Mining, Construction, Factories, Logging}
		//Q2 Array Order: {Drinking Water, Recreation, Swimming, Fishing, Industrial Water, Agriculture, Irrigation, Livestock Watering}
		//Q6 Array Order: {Dams, Bridges, Woody Debris, Waterfalls}
		int idsChk[][] = 
			{
				{R.id.chkC4Q1R_Farming, R.id.chkC4Q1R_Forest, R.id.chkC4Q1R_Residence, R.id.chkC4Q1R_PoultryOrSwine, R.id.chkC4Q1R_StoresOrMalls, 
					R.id.chkC4Q1R_Mining, R.id.chkC4Q1R_Construction, R.id.chkC4Q1R_Factories, R.id.chkC4Q1R_Logging},
					{R.id.chkC4Q2R_DrinkingWater, R.id.chkC4Q2R_Recreation, R.id.chkC4Q2R_Swimming, R.id.chkC4Q2R_Fishing, 
						R.id.chkC4Q2R_IndustrialWater, R.id.chkC4Q2R_Agriculture, R.id.chkC4Q2R_Irrigation, R.id.chkC4Q2R_Livestock},
						{R.id.chkC4Q6R_Dams, R.id.chkC4Q6R_Bridges, R.id.chkC4Q6R_Debris, R.id.chkC4Q6R_Waterfalls}
			};
		boolean valuesChk[][] = 
			{
				{answers.LandUse.farming, answers.LandUse.forest, answers.LandUse.residential, answers.LandUse.poultrySwine, answers.LandUse.stores, 
					answers.LandUse.mining, answers.LandUse.construction, answers.LandUse.factories, answers.LandUse.logging},
					{answers.StreamUse.drinking, answers.StreamUse.recreation, answers.StreamUse.swimming, answers.StreamUse.fishing, 
						answers.StreamUse.industrial, answers.StreamUse.agriculture, answers.StreamUse.irrigation, answers.StreamUse.livestock},
						{answers.Barriers.dams, answers.Barriers.bridges, answers.Barriers.woodyDebris, answers.Barriers.waterfalls}
			};

		//Q3:  Pipes in stream?
		//Q4:  Pipes dumping stuff?
		//Q5:  Trash in stream?
		int idsRdo[][] = 
			{
				{R.id.rdoC4Q3R_Yes, R.id.rdoC4Q3R_No},
				{R.id.rdoC4Q4R_Yes, R.id.rdoC4Q4R_No, R.id.rdoC4Q4R_NotApplicable},
				{R.id.rdoC4Q5R_Absent, R.id.rdoC4Q5R_Some, R.id.rdoC4Q5R_Abundant}
			};
		String valuesRdo[] = {answers.pipes, answers.pipeSecretion, answers.trash};

		loadCheckBoxAnswers(idsChk, valuesChk);
		loadRadioButtonAnswers(idsRdo, valuesRdo);

		Button button = (Button) findViewById(R.id.photo1Cat4);
		Bitmap bitmap;
		photoUri4 = answers.imageUriCat4;
		contentUri = Uri.parse(photoUri4);
		try {
			bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentUri);
			bitmap = Bitmap.createScaledBitmap(bitmap, 66, 48, false);
			BitmapDrawable bitDraw = new BitmapDrawable(getResources(), bitmap);
			button.setHeight(48);
			button.setWidth(66);
			button.setText(null);
			button.setBackground(bitDraw);
			pictureStatus = 1;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void loadCategoryThree(Observation answers) {

		//Q1 Array Order: {None, In Spots, On Rocks, Everywhere, Floating, Bottom of Streambed}
		//Q2 Array Order: {Light Green, Dark Green, Brown, Red, Orange}
		int idsChk[][] = 
			{
				{R.id.chkC3Q1R_None, R.id.chkC3Q1R_InSpots, R.id.chkC3Q1R_OnRocks, R.id.chkC3Q1R_Everywhere, R.id.chkC3Q1R_Floating, R.id.chkC3Q1R_OnStreambed},
				{R.id.chkC3Q2R_LightGreen, R.id.chkC3Q2R_DarkGreen, R.id.chkC3Q2R_Brown, R.id.chkC3Q2R_Red, R.id.chkC3Q2R_Orange}
			};
		boolean valuesChk[][] =
			{
				{answers.Algea.notPresent, answers.Algea.presentInSpots, answers.Algea.attachedToRocks, answers.Algea.everywhere, answers.Algea.floating, answers.Algea.mattedOnTheStreamBed},
				{answers.AlgeaColor.lightGreent, answers.AlgeaColor.darkGreen, answers.AlgeaColor.brown, answers.AlgeaColor.red, answers.AlgeaColor.orange}
			};

		//Q3:  Fish present?
		//Q4:  Number of types of fish?
		//Q5:  Other reptiles and stuff?
		//Q6:  Shade coverage?
		//Q7:  Left bank coverage of flora/fauna?
		//Q8:  Right bank coverage of flora/fauna?
		int idsRdo[][] =
			{
				{R.id.rdoC3Q3R_Absent, R.id.rdoC3Q3R_Some, R.id.rdoC3Q3R_Abundant},
				{R.id.rdoC3Q4R_One, R.id.rdoC3Q4R_TwoToFour, R.id.rdoC3Q4R_MoreThanFour},
				{R.id.rdoC3Q5R_Yes, R.id.rdoC3Q5R_No},
				{R.id.rdoC3Q6R_FullShade, R.id.rdoC3Q6R_MoreThanHalf, R.id.rdoC3Q6R_LessThanHalf, R.id.rdoC3Q6R_None},
				{R.id.rdoC3Q7R_Covered, R.id.rdoC3Q7R_MoreThanHalf, R.id.rdoC3Q7R_LessThanHalf, R.id.rdoC3Q7R_None},
				{R.id.rdoC3Q8R_Covered, R.id.rdoC3Q8R_MoreThanHalf, R.id.rdoC3Q8R_LessThanHalf, R.id.rdoC3Q8R_None}
			};
		String valuesRdo[] = {answers.fishPrescence, answers.fishTypes, answers.repAmphPresent, answers.treeSahde, answers.leftBank, answers.rightBank};

		loadCheckBoxAnswers(idsChk, valuesChk);
		loadRadioButtonAnswers(idsRdo, valuesRdo);

		Button button = (Button) findViewById(R.id.photo1Cat3);
		Bitmap bitmap;
		photoUri3 = answers.imageUriCat3;
		contentUri = Uri.parse(photoUri3);
		try {
			bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentUri);
			bitmap = Bitmap.createScaledBitmap(bitmap, 66, 48, false);
			BitmapDrawable bitDraw = new BitmapDrawable(getResources(), bitmap);
			button.setHeight(48);
			button.setWidth(66);
			button.setText(null);
			button.setBackground(bitDraw);
			pictureStatus = 1;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadCategoryTwo(Observation answers) {

		//Q1 Array Order:  {Clear, Oily, Black, Foamy, Green, Muddy, Brown, Other}
		//Q2 Array Order:  {No Odor, Rotten Egg, Gasoline or Oil, Chemical, Chlorine, Sewage, Rotting, Fishy}
		//Q5 Array Order:  {Sand, Gravel, Boulders, Bedrock, Silt, Other}
		int idsChk[][] =
			{
				{R.id.chkC2Q1R_Clear, R.id.chkC2Q1R_Oily, R.id.chkC2Q1R_Black, R.id.chkC2Q1R_Foamy,
					R.id.chkC2Q1R_Green, R.id.chkC2Q1R_Muddy, R.id.chkC2Q1R_Brown, R.id.chkC2Q1R_Other},
					{R.id.chkC2Q2R_NoOdor, R.id.chkC2Q2R_RottenEgg, R.id.chkC2Q2R_GasolineOil, R.id.chkC2Q2R_Chemical,
						R.id.chkC2Q2R_Chlorine, R.id.chkC2Q2R_Sewage, R.id.chkC2Q2R_Rotting, R.id.chkC2Q2R_Fishy},
						{R.id.chkC2Q5R_Sand, R.id.chkC2Q5R_Gravel, R.id.chkC2Q5R_Boulders, R.id.chkC2Q5R_Bedrock, R.id.chkC2Q5R_Silt, R.id.chkC2Q5R_Other}
			};
		boolean valuesChk[][] =
			{
				{answers.Color.clear, answers.Color.oily, answers.Color.balck, answers.Color.foamy,
					answers.Color.green, answers.Color.muddy, answers.Color.brown, answers.Color.other},
					{answers.Smell.noOdor, answers.Smell.rottenEggs, answers.Smell.gasolineOil, answers.Smell.chemical,
						answers.Smell.chlronine, answers.Smell.sewage, answers.Smell.rotting, answers.Smell.fishy},
						{answers.Composition.sand, answers.Composition.gravel, answers.Composition.boulders,
							answers.Composition.manMadeCement, answers.Composition.silt, answers.Composition.other}
			};

		//Q3:  Water flow speed?
		//Q4:  Streamwalk flow speed?
		int idsRdo[][] = 
			{
				{R.id.rdoC2Q3R_VeryFast, R.id.rdoC2Q3R_Fast, R.id.rdoC2Q3R_Slow, R.id.rdoC2Q3R_VerySlow},
				{R.id.rdoC2Q4R_MostlyFast, R.id.rdoC2Q4R_SpeedVaries, R.id.rdoC2Q4R_MostlySlow}
			};
		String valuesRdo[] = {answers.waterSpeed, answers.waterMovement};

		loadCheckBoxAnswers(idsChk, valuesChk);
		loadRadioButtonAnswers(idsRdo, valuesRdo);

		Button button = (Button) findViewById(R.id.photo1Cat2);
		Bitmap bitmap;
		photoUri2 = answers.imageUriCat2;
		contentUri = Uri.parse(photoUri2);
		try {
			bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentUri);
			bitmap = Bitmap.createScaledBitmap(bitmap, 66, 48, false);
			BitmapDrawable bitDraw = new BitmapDrawable(getResources(), bitmap);
			button.setHeight(48);
			button.setWidth(66);
			button.setText(null);
			button.setBackground(bitDraw);
			pictureStatus = 1;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void loadCategoryOne(Observation answers) {
		int ids[] = {R.id.date, R.id.waterbody, R.id.watershedName, R.id.state, R.id.longitude, R.id.latitude, R.id.photo1Cat1};
		String values[] = {answers.date, answers.waterbody, answers.watershed, answers.stateMuni, answers.locationLong, answers.locationLat, answers.imageUriCat1};

		EditText text;

		for (int i=0; i<ids.length; i++)
		{
			if (i == ids.length-1) {
				Button button = (Button) findViewById(R.id.photo1Cat1);
				Bitmap bitmap;
				photoUri1 = values[6];
				contentUri = Uri.parse(photoUri1);
				Log.d(TAG, "photoUri1: " + photoUri1);
				Log.d(TAG, "contentUri: " + contentUri.toString());
				if (!contentUri.toString().equals(""))
					pictureStatus = 1;
				try {
					bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentUri);
					bitmap = Bitmap.createScaledBitmap(bitmap, 66, 48, false);
					BitmapDrawable bitDraw = new BitmapDrawable(getResources(), bitmap);
					button.setHeight(48);
					button.setWidth(66);
					button.setText("");
					button.setBackground(bitDraw);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			else {
				text = (EditText) findViewById(ids[i]);
				text.setText(values[i]);
			}
		}

	}

	public void showView(int section)
	{
		RelativeLayout cat1 = (RelativeLayout) findViewById(R.id.category1);
		RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
		RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
		RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
		RelativeLayout cat5 = (RelativeLayout) findViewById(R.id.category5);

		cat1.setVisibility(View.GONE);
		cat2.setVisibility(View.GONE);
		cat3.setVisibility(View.GONE);
		cat4.setVisibility(View.GONE);
		cat5.setVisibility(View.GONE);

		currentSection = section;

		switch(section){
		case 1: cat2PrevOnClick(null);
		break;
		case 2: cat1NextOnClick(null); 
		break;
		case 3: cat2NextOnClick(null);
		break;
		case 4: cat3NextOnClick(null);
		break;
		case 5: cat4NextOnClick(null);
		break;
		}
	}

	@Override
	public void onBackPressed() {
		if(currentSection > 1)
		{
			showView(currentSection -1);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		menu.clear();
		switch(currentSection){
		case 1: 
			menu.add(0, 1, 0, "Help");
			break;
		case 2: menu.add(0, 2, 0, "Help"); 
		break;
		case 3: menu.add(0, 3, 0, "Help"); 
		break;
		case 4: menu.add(0, 4, 0, "Help"); 
		break;
		case 5:menu.add(0, 5, 0, "Help"); 
		break;
		}
		getMenuInflater().inflate(R.menu.questionnaire, menu);
		return true;
	}

	public void rdoCol1OnClick(View v) {
		RadioGroup column2 = (RadioGroup) findViewById(R.id.rdoC2Q3R_SlowGroup);
		column2.clearCheck();
	}

	public void rdoCol2OnClick(View v) {
		RadioGroup column1 = (RadioGroup) findViewById(R.id.rdoC2Q3R_FastGroup);
		column1.clearCheck();
	}

	public void FullScreenOnClick(View v) {
		/*case switch
		 * set visibilities
		 * imageview
		 * fullscreen
		 * click sends back
		 */

		if (currentSection == 1) {
			RelativeLayout cat1 = (RelativeLayout) findViewById(R.id.category1);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			cat1.setVisibility(View.GONE);
			fs.setVisibility(View.VISIBLE);

		} else if (currentSection == 2) {
			RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			cat2.setVisibility(View.GONE);
			fs.setVisibility(View.VISIBLE);

		} else if (currentSection == 3) {
			RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			cat3.setVisibility(View.GONE);
			fs.setVisibility(View.VISIBLE);

		} else if (currentSection == 4) {
			RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
			RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

			cat4.setVisibility(View.GONE);
			fs.setVisibility(View.VISIBLE);
		}

		ImageView i = (ImageView) findViewById(R.id.fullPhoto);
		switch(currentSection){
		case 1: i.setImageURI(Uri.parse(photoUri1));
		break;
		case 2: i.setImageURI(Uri.parse(photoUri2)); 
		break;
		case 3: i.setImageURI(Uri.parse(photoUri3)); 
		break;
		case 4: i.setImageURI(Uri.parse(photoUri4)); 
		break;
		}
		i.setAdjustViewBounds(true);
		i.setClickable(true);
		i.setVisibility(View.VISIBLE);
		i.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				if (currentSection == 1) {
					RelativeLayout cat1 = (RelativeLayout) findViewById(R.id.category1);
					RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

					fs.setVisibility(View.GONE);
					cat1.setVisibility(View.VISIBLE);

				} else if (currentSection == 2) {
					RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
					RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

					fs.setVisibility(View.GONE);
					cat2.setVisibility(View.VISIBLE);

				} else if (currentSection == 3) {
					RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
					RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

					fs.setVisibility(View.GONE);
					cat3.setVisibility(View.VISIBLE);

				} else if (currentSection == 4) {
					RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
					RelativeLayout fs = (RelativeLayout) findViewById(R.id.fullScreen);

					fs.setVisibility(View.GONE);
					cat4.setVisibility(View.VISIBLE);

				}

			}
		});

	}

	public void cat1NextOnClick(View v) {
		RelativeLayout cat1 = (RelativeLayout) findViewById(R.id.category1);
		RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat1.setVisibility(View.GONE);
		cat2.setVisibility(View.VISIBLE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 2;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat2));

		Button picBtn = (Button) findViewById(R.id.photo1Cat2);
		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

		if (picBtn.getText().toString().equals(getString(R.string.photo)))
			pictureStatus = 0;
		else
			pictureStatus = 1;
	}

	public void cat2PrevOnClick(View v) {
		RelativeLayout cat1 = (RelativeLayout) findViewById(R.id.category1);
		RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat1.setVisibility(View.VISIBLE);
		cat2.setVisibility(View.GONE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 1;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat1));

		Button picBtn = (Button) findViewById(R.id.photo1Cat1);
		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

		WatershedDbAdapter db = new WatershedDbAdapter(this);
		db.open();
		Observation ans = db.getObservation(observationId);

		Log.d(TAG, "picBtnText: " + picBtn.getText());
		Log.d(TAG, "RstringPhoto: " + getString(R.string.photo));
		Log.d(TAG, "ans.imageuricat1: " + ans.imageUriCat1);



		if (ans.imageUriCat1.equals("")) {
			pictureStatus = 0;
			Log.d(TAG, "PICTURE STATUS 0");
		}
		else {
			pictureStatus = 1;
			Log.d(TAG, "PICTURE STATUS 1");
		}

		db.close();

	}

	public void cat2NextOnClick(View v) {
		RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
		RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat2.setVisibility(View.GONE);
		cat3.setVisibility(View.VISIBLE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 3;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat3));

		Button picBtn = (Button) findViewById(R.id.photo1Cat3);
		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

		if (picBtn.getText().toString().equals(getString(R.string.photo)))
			pictureStatus = 0;
		else
			pictureStatus = 1;
	}

	public void cat3PrevOnClick(View v) {
		RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
		RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat2.setVisibility(View.VISIBLE);
		cat3.setVisibility(View.GONE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 2;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat2));

		Button picBtn = (Button) findViewById(R.id.photo1Cat2);
		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

		if (picBtn.getText().toString().equals(getString(R.string.photo)))
			pictureStatus = 0;
		else
			pictureStatus = 1;
	}

	public void cat3NextOnClick(View v) {
		RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
		RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat4.setVisibility(View.VISIBLE);
		cat3.setVisibility(View.GONE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 4;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat4));

		Button picBtn = (Button) findViewById(R.id.photo1Cat4);
		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

		if (picBtn.getText().toString().equals(getString(R.string.photo)))
			pictureStatus = 0;
		else
			pictureStatus = 1;
	}

	public void cat4PrevOnClick(View v) {
		RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
		RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat3.setVisibility(View.VISIBLE);
		cat4.setVisibility(View.GONE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 3;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat3));

		Button picBtn = (Button) findViewById(R.id.photo1Cat3);
		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

		if (picBtn.getText().toString().equals(getString(R.string.photo)))
			pictureStatus = 0;
		else
			pictureStatus = 1;
	}

	public void cat4NextOnClick(View v) {
		RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
		RelativeLayout cat5 = (RelativeLayout) findViewById(R.id.category5);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat4.setVisibility(View.GONE);
		cat5.setVisibility(View.VISIBLE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 5;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat5));
	}

	public void cat5PrevOnClick(View v) {
		RelativeLayout cat5 = (RelativeLayout) findViewById(R.id.category5);
		RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
		ScrollView sv = (ScrollView) findViewById(R.id.QuestionnaireScroll);
		cat4.setVisibility(View.VISIBLE);
		cat5.setVisibility(View.GONE);
		sv.scrollTo(0, sv.getTop());
		currentSection = 4;
		ActivityCompat.invalidateOptionsMenu(this);
		setTitle(getString(R.string.lblCat4));

		Button picBtn = (Button) findViewById(R.id.photo1Cat4);
		setBtnListenerOrDisable( 
				picBtn, 
				mTakePicOnClickListener,
				mDeletePicOnLongClickListener,
				MediaStore.ACTION_IMAGE_CAPTURE
				);

		if (picBtn.getText().toString().equals(getString(R.string.photo)))
			pictureStatus = 0;
		else
			pictureStatus = 1;
	}

	public void reviewOnClick(View v) {
		Intent overview = new Intent(this, ObservationOverview.class);		

		Observation answers = new Observation();
		packCategoryOneResponses(answers);
		packCategoryTwoResponses(answers);
		packCategoryThreeResponses(answers);
		packCategoryFourResponses(answers);
		packCategoryFiveResponses(answers);

		WatershedDbAdapter db = new WatershedDbAdapter(this);
		answers.rowId = observationId;
		db.open().saveObservation(answers);
		db.close();

		overview.putExtra("id", answers.rowId);

		startActivity(overview);
		finish();
	}

	private void packCategoryFiveResponses(Observation ans) {
		//Gather and package category 5 responses

		RadioGroup col1;
		int tempId;

		//Q1
		int anonymous;
		col1 = (RadioGroup) findViewById(R.id.rdoC5Q1R_Anonymous);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			anonymous = -1;
		}
		else
		{
			if (tempId == R.id.rdoC5Q1R_Yes)
			{
				anonymous = 1;
			}
			else
			{
				anonymous = 0;
			}
		}

		if (anonymous == 1)
		{
			ans.anonymous = true;			
		}
		else
		{
			ans.anonymous = false;
		}

		//Q2
		String comments = ((EditText) findViewById(R.id.comments)).getText().toString();

		ans.comments = comments;

	}

	private void packCategoryFourResponses(Observation ans) {
		//Gather and package category 4 responses

		RadioGroup col1;
		int tempId;

		//None of these actually need to use arrays anymore, but it would be wasted effort to remove them so....

		//Q1 Array Order: {Farming/Crops, Forest, Residential, Poultry/Swine, Stores/Malls, Mining, Construction, Factories, Logging}
		boolean c4q1LandUsage[] = new boolean[9];
		c4q1LandUsage[0] = ((CheckBox) findViewById(R.id.chkC4Q1R_Farming)).isChecked();
		c4q1LandUsage[1] = ((CheckBox) findViewById(R.id.chkC4Q1R_Forest)).isChecked();
		c4q1LandUsage[2] = ((CheckBox) findViewById(R.id.chkC4Q1R_Residence)).isChecked();
		c4q1LandUsage[3] = ((CheckBox) findViewById(R.id.chkC4Q1R_PoultryOrSwine)).isChecked();
		c4q1LandUsage[4] = ((CheckBox) findViewById(R.id.chkC4Q1R_StoresOrMalls)).isChecked();
		c4q1LandUsage[5] = ((CheckBox) findViewById(R.id.chkC4Q1R_Mining)).isChecked();
		c4q1LandUsage[6] = ((CheckBox) findViewById(R.id.chkC4Q1R_Construction)).isChecked();
		c4q1LandUsage[7] = ((CheckBox) findViewById(R.id.chkC4Q1R_Factories)).isChecked();
		c4q1LandUsage[8] = ((CheckBox) findViewById(R.id.chkC4Q1R_Logging)).isChecked();

		ans.LandUse.farming = c4q1LandUsage[0];
		ans.LandUse.forest = c4q1LandUsage[1];
		ans.LandUse.residential = c4q1LandUsage[2];
		ans.LandUse.poultrySwine = c4q1LandUsage[3];
		ans.LandUse.stores = c4q1LandUsage[4];
		ans.LandUse.mining = c4q1LandUsage[5];
		ans.LandUse.construction = c4q1LandUsage[6];
		ans.LandUse.factories = c4q1LandUsage[7];
		ans.LandUse.logging = c4q1LandUsage[8];

		//Q2 Array Order: {Drinking Water, Recreation, Swimming, Fishing, Industrial Water, Agriculture, Irrigation, Livestock Watering}
		boolean c4q2StreamUsage[] = new boolean[8];
		c4q2StreamUsage[0] = ((CheckBox) findViewById(R.id.chkC4Q2R_DrinkingWater)).isChecked();
		c4q2StreamUsage[1] = ((CheckBox) findViewById(R.id.chkC4Q2R_Recreation)).isChecked();
		c4q2StreamUsage[2] = ((CheckBox) findViewById(R.id.chkC4Q2R_Swimming)).isChecked();
		c4q2StreamUsage[3] = ((CheckBox) findViewById(R.id.chkC4Q2R_Fishing)).isChecked();
		c4q2StreamUsage[4] = ((CheckBox) findViewById(R.id.chkC4Q2R_IndustrialWater)).isChecked();
		c4q2StreamUsage[5] = ((CheckBox) findViewById(R.id.chkC4Q2R_Agriculture)).isChecked();
		c4q2StreamUsage[6] = ((CheckBox) findViewById(R.id.chkC4Q2R_Irrigation)).isChecked();
		c4q2StreamUsage[7] = ((CheckBox) findViewById(R.id.chkC4Q2R_Livestock)).isChecked();

		ans.StreamUse.drinking = c4q2StreamUsage[0];
		ans.StreamUse.recreation = c4q2StreamUsage[1];
		ans.StreamUse.swimming = c4q2StreamUsage[2];
		ans.StreamUse.fishing = c4q2StreamUsage[3];
		ans.StreamUse.industrial = c4q2StreamUsage[4];
		ans.StreamUse.agriculture = c4q2StreamUsage[5];
		ans.StreamUse.irrigation = c4q2StreamUsage[6];
		ans.StreamUse.livestock = c4q2StreamUsage[7];

		//Q3
		String pipesInStream;
		col1 = (RadioGroup) findViewById(R.id.rdoC4Q3R_PipesInStream);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			pipesInStream = "";
		}
		else
		{
			pipesInStream = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.pipes = pipesInStream;

		//Q4
		String pipesDumping;
		col1 = (RadioGroup) findViewById(R.id.rdoC4Q4R_PipesDumping);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			pipesDumping = "";
		}
		else
		{
			pipesDumping = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.pipeSecretion = pipesDumping;

		//Q5
		String trash;
		col1 = (RadioGroup) findViewById(R.id.rdoC4Q5R_Trash);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			trash = "";
		}
		else
		{
			trash = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.trash = trash;

		//Q6 Array Order: {Dams, Bridges, Woody Debris, Waterfalls}
		boolean c4q6StreamBarriers[] = new boolean[4];
		c4q6StreamBarriers[0] = ((CheckBox) findViewById(R.id.chkC4Q6R_Dams)).isChecked();
		c4q6StreamBarriers[1] = ((CheckBox) findViewById(R.id.chkC4Q6R_Bridges)).isChecked();
		c4q6StreamBarriers[2] = ((CheckBox) findViewById(R.id.chkC4Q6R_Debris)).isChecked();
		c4q6StreamBarriers[3] = ((CheckBox) findViewById(R.id.chkC4Q6R_Waterfalls)).isChecked();

		ans.Barriers.dams = c4q6StreamBarriers[0];
		ans.Barriers.bridges = c4q6StreamBarriers[1];
		ans.Barriers.woodyDebris = c4q6StreamBarriers[2];
		ans.Barriers.waterfalls = c4q6StreamBarriers[3];

		// Image
		ans.imageUriCat4 = photoUri4;
		Log.d(TAG, "photoUri4 = " + photoUri4);

	}

	private void packCategoryThreeResponses(Observation ans) {
		//Gather and package category 3 responses

		RadioGroup col1;
		int tempId;

		//Q1 Array Order: {None, In Spots, On Rocks, Everywhere, Floating, Bottom of Streambed}
		boolean c3q1AlgaePresence[] = new boolean[6];
		c3q1AlgaePresence[0] = ((CheckBox) findViewById(R.id.chkC3Q1R_None)).isChecked();
		c3q1AlgaePresence[1] = ((CheckBox) findViewById(R.id.chkC3Q1R_InSpots)).isChecked();
		c3q1AlgaePresence[2] = ((CheckBox) findViewById(R.id.chkC3Q1R_OnRocks)).isChecked();
		c3q1AlgaePresence[3] = ((CheckBox) findViewById(R.id.chkC3Q1R_Everywhere)).isChecked();
		c3q1AlgaePresence[4] = ((CheckBox) findViewById(R.id.chkC3Q1R_Floating)).isChecked();
		c3q1AlgaePresence[5] = ((CheckBox) findViewById(R.id.chkC3Q1R_OnStreambed)).isChecked();

		ans.Algea.notPresent = c3q1AlgaePresence[0];
		ans.Algea.presentInSpots = c3q1AlgaePresence[1];
		ans.Algea.attachedToRocks = c3q1AlgaePresence[2];
		ans.Algea.everywhere = c3q1AlgaePresence[3];
		ans.Algea.floating = c3q1AlgaePresence[4];
		ans.Algea.mattedOnTheStreamBed = c3q1AlgaePresence[5];

		//Q2 Array Order: {Light Green, Dark Green, Brown, Red, Orange}
		boolean c3q2AlgaeColor[] = new boolean[5];
		c3q2AlgaeColor[0] = ((CheckBox) findViewById(R.id.chkC3Q2R_LightGreen)).isChecked();
		c3q2AlgaeColor[1] = ((CheckBox) findViewById(R.id.chkC3Q2R_DarkGreen)).isChecked();
		c3q2AlgaeColor[2] = ((CheckBox) findViewById(R.id.chkC3Q2R_Brown)).isChecked();
		c3q2AlgaeColor[3] = ((CheckBox) findViewById(R.id.chkC3Q2R_Red)).isChecked();
		c3q2AlgaeColor[4] = ((CheckBox) findViewById(R.id.chkC3Q2R_Orange)).isChecked();

		ans.AlgeaColor.lightGreent = c3q2AlgaeColor[0];
		ans.AlgeaColor.darkGreen = c3q2AlgaeColor[1];
		ans.AlgeaColor.brown = c3q2AlgaeColor[2];
		ans.AlgeaColor.red = c3q2AlgaeColor[3];
		ans.AlgeaColor.orange = c3q2AlgaeColor[4];

		//Q3
		String fishPresence;
		col1 = (RadioGroup) findViewById(R.id.rdoC3Q3R_FishPresence);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			fishPresence = "";
		}
		else
		{
			fishPresence = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.fishPrescence = fishPresence;

		//Q4
		String numFishTypes;
		col1 = (RadioGroup) findViewById(R.id.rdoC3Q4R_FishDiversity);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			numFishTypes = "";
		}
		else
		{
			numFishTypes = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.fishTypes = numFishTypes;

		//Q5
		String otherAnimalLife;
		col1 = (RadioGroup) findViewById(R.id.rdoC3Q5R_OtherAnimalLife);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			otherAnimalLife = "";
		}
		else
		{
			otherAnimalLife = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.repAmphPresent = otherAnimalLife;

		//Q6
		String shadeCoverage;
		col1 = (RadioGroup) findViewById(R.id.rdoC3Q6R_ShadeCoverage);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			shadeCoverage = "";
		}
		else
		{
			shadeCoverage = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.treeSahde = shadeCoverage;

		//Q7
		String leftBankDetail;
		col1 = (RadioGroup) findViewById(R.id.rdoC3Q7R_LeftBankDetail);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			leftBankDetail = "";
		}
		else
		{
			leftBankDetail = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.leftBank = leftBankDetail;

		//Q8
		String rightBankDetail;
		col1 = (RadioGroup) findViewById(R.id.rdoC3Q8R_RightBankDetail);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			rightBankDetail = "";
		}
		else
		{
			rightBankDetail = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.rightBank = rightBankDetail;


		// Image
		ans.imageUriCat3 = photoUri3;
		Log.d(TAG, "photoUri3 = " + photoUri3);


	}

	private void packCategoryTwoResponses(Observation ans) {
		//Gather and package category 2 responses

		//Q1 Array Order:  {Clear, Oily, Black, Foamy, Green, Muddy, Brown, Other}
		boolean c2q1WaterColor[] = new boolean[8];
		c2q1WaterColor[0] = ((CheckBox) findViewById(R.id.chkC2Q1R_Clear)).isChecked();
		c2q1WaterColor[1] = ((CheckBox) findViewById(R.id.chkC2Q1R_Oily)).isChecked();
		c2q1WaterColor[2] = ((CheckBox) findViewById(R.id.chkC2Q1R_Black)).isChecked();
		c2q1WaterColor[3] = ((CheckBox) findViewById(R.id.chkC2Q1R_Foamy)).isChecked();
		c2q1WaterColor[4] = ((CheckBox) findViewById(R.id.chkC2Q1R_Green)).isChecked();
		c2q1WaterColor[5] = ((CheckBox) findViewById(R.id.chkC2Q1R_Muddy)).isChecked();
		c2q1WaterColor[6] = ((CheckBox) findViewById(R.id.chkC2Q1R_Brown)).isChecked();
		c2q1WaterColor[7] = ((CheckBox) findViewById(R.id.chkC2Q1R_Other)).isChecked();

		ans.Color.clear = c2q1WaterColor[0];
		ans.Color.oily = c2q1WaterColor[1];
		ans.Color.balck = c2q1WaterColor[2];
		ans.Color.foamy = c2q1WaterColor[3];
		ans.Color.green = c2q1WaterColor[4];
		ans.Color.muddy = c2q1WaterColor[5];
		ans.Color.brown = c2q1WaterColor[6];
		ans.Color.other = c2q1WaterColor[7];

		//Q2 Array Order: {No Odor, Rotten Egg, Gasoline or Oil, Chemical, Chlorine, Sewage, Rotting, Fishy}
		boolean c2q2WaterSmell[] = new boolean[8];
		c2q2WaterSmell[0] = ((CheckBox) findViewById(R.id.chkC2Q2R_NoOdor)).isChecked();
		c2q2WaterSmell[1] = ((CheckBox) findViewById(R.id.chkC2Q2R_RottenEgg)).isChecked();
		c2q2WaterSmell[2] = ((CheckBox) findViewById(R.id.chkC2Q2R_GasolineOil)).isChecked();
		c2q2WaterSmell[3] = ((CheckBox) findViewById(R.id.chkC2Q2R_Chemical)).isChecked();
		c2q2WaterSmell[4] = ((CheckBox) findViewById(R.id.chkC2Q2R_Chlorine)).isChecked();
		c2q2WaterSmell[5] = ((CheckBox) findViewById(R.id.chkC2Q2R_Sewage)).isChecked();
		c2q2WaterSmell[6] = ((CheckBox) findViewById(R.id.chkC2Q2R_Rotting)).isChecked();
		c2q2WaterSmell[7] = ((CheckBox) findViewById(R.id.chkC2Q2R_Fishy)).isChecked();

		ans.Smell.noOdor = c2q2WaterSmell[0];
		ans.Smell.rottenEggs = c2q2WaterSmell[1];
		ans.Smell.gasolineOil = c2q2WaterSmell[2];
		ans.Smell.chemical = c2q2WaterSmell[3];
		ans.Smell.chlronine = c2q2WaterSmell[4];
		ans.Smell.sewage = c2q2WaterSmell[5];
		ans.Smell.rotting = c2q2WaterSmell[6];
		ans.Smell.fishy = c2q2WaterSmell[7];

		//Q3  -This is messy because radio groups are stupid with layouts.  
		String waterSpeed;
		RadioGroup col1 = (RadioGroup) findViewById(R.id.rdoC2Q3R_FastGroup);
		RadioGroup col2 = (RadioGroup) findViewById(R.id.rdoC2Q3R_SlowGroup);
		RadioButton rdoTemp;

		int tempId = col1.getCheckedRadioButtonId();
		int tempId2 = col2.getCheckedRadioButtonId();

		if (tempId == -1 && tempId2 == -1)
		{
			waterSpeed = "";
		}
		else if (tempId != -1 && tempId2 != -1)
		{
			waterSpeed = "??? ERROR:  SOMETHING BAD HAPPENED ???";
		}
		else
		{
			if (tempId != -1)
			{
				rdoTemp = (RadioButton) findViewById(tempId);
			}
			else
			{
				rdoTemp = (RadioButton) findViewById(tempId2);
			}
			waterSpeed = rdoTemp.getText().toString();
		}

		ans.waterSpeed = waterSpeed;

		//Q4
		String streamWalkSpeed;
		col1 = (RadioGroup) findViewById(R.id.rdoC2Q4R_Speed);

		tempId = col1.getCheckedRadioButtonId();

		if (tempId == -1)
		{
			streamWalkSpeed = "";
		}
		else
		{
			streamWalkSpeed = ((RadioButton) findViewById(tempId)).getText().toString();
		}

		ans.waterMovement = streamWalkSpeed;

		//Q5 Array Order:  {Sand, Gravel, Boulders, Bedrock, Silt, Other}
		boolean c2q5Composition[] = new boolean[6];
		c2q5Composition[0] = ((CheckBox) findViewById(R.id.chkC2Q5R_Sand)).isChecked();
		c2q5Composition[1] = ((CheckBox) findViewById(R.id.chkC2Q5R_Gravel)).isChecked();
		c2q5Composition[2] = ((CheckBox) findViewById(R.id.chkC2Q5R_Boulders)).isChecked();
		c2q5Composition[3] = ((CheckBox) findViewById(R.id.chkC2Q5R_Bedrock)).isChecked();
		c2q5Composition[4] = ((CheckBox) findViewById(R.id.chkC2Q5R_Silt)).isChecked();
		c2q5Composition[5] = ((CheckBox) findViewById(R.id.chkC2Q5R_Other)).isChecked();

		ans.Composition.sand = c2q5Composition[0];
		ans.Composition.gravel = c2q5Composition[1];
		ans.Composition.boulders = c2q5Composition[2];
		ans.Composition.manMadeCement = c2q5Composition[3];
		ans.Composition.silt = c2q5Composition[4];
		ans.Composition.other = c2q5Composition[5];

		// Image
		ans.imageUriCat2 = photoUri2;
		Log.d(TAG, "photoUri2 = " + photoUri2);

	}

	private void packCategoryOneResponses(Observation ans) {

		//Gather and package category 1 responses
		String date = ((EditText) findViewById(R.id.date)).getText().toString();
		String waterbody = ((EditText) findViewById(R.id.waterbody)).getText().toString();
		String watershed = ((EditText) findViewById(R.id.watershedName)).getText().toString();
		String state = ((EditText) findViewById(R.id.state)).getText().toString();
		String longitude = ((EditText) findViewById(R.id.longitude)).getText().toString();
		String latitude = ((EditText) findViewById(R.id.latitude)).getText().toString();

		ans.date = date;
		ans.waterbody = waterbody;
		ans.watershed = watershed;
		ans.stateMuni = state;
		ans.locationLong = longitude;
		ans.locationLat = latitude;

		// Image
		ans.imageUriCat1 = photoUri1;
		Log.d(TAG, "photoUri1: " + photoUri1);
		Log.d(TAG, "imageUriCat1: " + ans.imageUriCat1);

	}

	public void removeView(View v){
		ViewGroup vg = (ViewGroup)(v.getParent());
		vg.removeView(v);
		vg.setVisibility(View.GONE);
		switch(currentSection){
		case 1:
			(findViewById(R.id.btnNextCat1)).setEnabled(true);
			break;
		case 2:
			(findViewById(R.id.btnNextCat2)).setEnabled(true);
			(findViewById(R.id.btnPreviousCat2)).setEnabled(true);
			break;
		case 3:
			(findViewById(R.id.btnNextCat3)).setEnabled(true);
			(findViewById(R.id.btnPreviousCat3)).setEnabled(true);
			break;
		case 4:
			(findViewById(R.id.btnNextCat4)).setEnabled(true);
			(findViewById(R.id.btnPreviousCat4)).setEnabled(true);
			break;
		case 5:
			(findViewById(R.id.btnReview)).setEnabled(true);
			(findViewById(R.id.btnPreviousCat5)).setEnabled(true);
			break;

		}


		Log.d("BLAH", "BLAHDLDHFDK");

	}

	public boolean onMenuItemSelected(int id, MenuItem item){
		Log.d("BLAH", id +"");
		switch(item.getItemId()){
		case 1:
			RelativeLayout cat1 = (RelativeLayout) findViewById(R.id.category1);
			(findViewById(R.id.btnNextCat1)).setEnabled(false);

			View.inflate(this, R.layout.cat1help, cat1);
			return true;
		case 2: 
			RelativeLayout cat2 = (RelativeLayout) findViewById(R.id.category2);
			View.inflate(this, R.layout.cat2help, cat2);

			(findViewById(R.id.btnNextCat2)).setEnabled(false);
			(findViewById(R.id.btnPreviousCat2)).setEnabled(false);
			return true; 
		case 3:
			RelativeLayout cat3 = (RelativeLayout) findViewById(R.id.category3);
			View.inflate(this, R.layout.cat3help, cat3);
			(findViewById(R.id.btnNextCat3)).setEnabled(false);
			(findViewById(R.id.btnPreviousCat3)).setEnabled(false);
			return true;
		case 4:
			RelativeLayout cat4 = (RelativeLayout) findViewById(R.id.category4);
			View.inflate(this, R.layout.cat4help, cat4);
			(findViewById(R.id.btnNextCat4)).setEnabled(false);
			(findViewById(R.id.btnPreviousCat4)).setEnabled(false);
			return true;
		case 5:
			RelativeLayout cat5 = (RelativeLayout) findViewById(R.id.category5);
			View.inflate(this, R.layout.cat5help, cat5);
			(findViewById(R.id.btnReview)).setEnabled(false);
			(findViewById(R.id.btnPreviousCat5)).setEnabled(false);
			return true;
		}
		return false;
	}

	@Override
	public void finish(){
		currentSection = -1;
		super.finish();
	}

	/* Class My Location Listener */



	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		EditText longText = (EditText) findViewById(R.id.longitude);
		EditText latText = (EditText) findViewById(R.id.latitude);

		longText.setText(Double.toString(location.getLongitude()));
		latText.setText(Double.toString(location.getLatitude()));
		locationManager.removeUpdates(mListener);
	}
	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
